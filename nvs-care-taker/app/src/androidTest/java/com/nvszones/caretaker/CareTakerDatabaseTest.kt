package com.nvszones.caretaker

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.nvszones.caretaker.dao.CareTakerDatabase
import com.nvszones.caretaker.dao.TripsDao
import com.nvszones.caretaker.models.Trip
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class CareTakerDatabaseTest {

    private lateinit var tripsDao: TripsDao
    private lateinit var db: CareTakerDatabase

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext

        // Using in memory database
        db = Room.inMemoryDatabaseBuilder(context, CareTakerDatabase::class.java)
            .allowMainThreadQueries()
            .build()
        tripsDao = db.tripsDao
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun insertAndGetTrip() {
        val id = 1L
        val targetTrip = Trip(id, "Trip 1", "Route 1", Trip.TRIP_TYPE_PICKUP, "2019-10-22T01:30:00", "2019-10-22T02:30:00")
        tripsDao.insert(targetTrip)
        val trip = tripsDao.get(id)
        assert(trip == targetTrip)
    }
}