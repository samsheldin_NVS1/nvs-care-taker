package com.nvszones.caretaker

import android.content.Context
import android.content.pm.PackageManager
import android.content.pm.PackageManager.GET_META_DATA
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        resetTitles()

    }
    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LanguagePreference.setLocale(base))
    }
    private fun resetTitles() {
        try
        {
            val info = getPackageManager().getActivityInfo(getComponentName(), GET_META_DATA)
            if (info.labelRes != 0)//  !== instead of >
            {
                setTitle(info.labelRes)
            }
        }
        catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }


}
