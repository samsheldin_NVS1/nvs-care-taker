package com.nvszones.caretaker

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.util.Log
import androidx.annotation.StringDef
import androidx.preference.PreferenceManager
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.RetentionPolicy.*
import java.util.*


/**
 * SharedPreferences Key
 */


class LanguagePreference(context: Context) {
    @Retention(AnnotationRetention.SOURCE)
    @StringDef(LANGUAGE_KEY_ENGLISH, LANGUAGE_KEY_HINDI, LANGUAGE_KEY_KANNADA,LANGUAGE_KEY_TAMIL,LANGUAGE_KEY_TELUGU)
    annotation class LocaleDef {
        companion object {
            val SUPPORTED_LOCALES = arrayOf<String>(LANGUAGE_KEY_ENGLISH, LANGUAGE_KEY_HINDI, LANGUAGE_KEY_KANNADA,LANGUAGE_KEY_TAMIL,LANGUAGE_KEY_TELUGU)
        }
    }

    companion object {
        const val LANGUAGE_KEY_ENGLISH = "en"

        const val LANGUAGE_KEY_HINDI = "hi"

        const val LANGUAGE_KEY_KANNADA = "kn"

        const val LANGUAGE_KEY_TAMIL = "ta"

        const val LANGUAGE_KEY_TELUGU = "te"

        private const val LANGUAGE_KEY = "language_key"

        fun setLocale(mContext: Context): Context {
            return updateResources(
                mContext,
                getLanguagePref(mContext)
            )
        }

        /**
         * Set new Locale with context
         */
        fun setNewLocale(mContext: Context, @LocaleDef mLocaleKey: String): Context {
            setLanguagePref(mContext, mLocaleKey)
            return updateResources(mContext, mLocaleKey)
        }

        /**
         * Get saved Locale from SharedPreferences
         * @return current locale key by default return english locale
         */
        private fun getLanguagePref(mContext: Context): String? {
            val mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext)
            return mPreferences.getString(
                LANGUAGE_KEY,
                LANGUAGE_KEY_ENGLISH
            )
        }

        /**
         * set pref key
         * @param mContext
         * @param localeKey
         */
        private fun setLanguagePref(mContext: Context, localeKey: String) {
            val mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext)
            mPreferences.edit().putString(LANGUAGE_KEY, localeKey).apply()
        }

        private fun updateResources(context: Context, language: String?): Context {
            val locale = Locale(language)
            Locale.setDefault(locale)
            val res = context.getResources()
            val config = Configuration(res.getConfiguration())
            if (Build.VERSION.SDK_INT >= 17) {
                config.setLocale(locale)
                return context.createConfigurationContext(config)
            } else {
                config.locale = locale
                res.updateConfiguration(config, res.getDisplayMetrics())
            }

            return context
        }
         fun getLocale(res: Resources):Locale {
             val config = res.getConfiguration()
             return if (Build.VERSION.SDK_INT >= 24) config.getLocales().get(0) else config.locale
         }
    }
}