package com.nvszones.caretaker

import android.Manifest
import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.*
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.telephony.TelephonyManager
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.preference.PreferenceManager
import com.google.android.material.snackbar.Snackbar
import com.nvszones.caretaker.dao.CareTakerDatabase
import com.nvszones.caretaker.models.Trip
import com.nvszones.caretaker.viewmodels.*
import kotlinx.android.synthetic.main.layout_app_bar.*
import kotlinx.android.synthetic.main.layout_app_bar.view.*
import timber.log.Timber
import java.util.*

const val SYNC_FREQUENCY = 60 * 1000L // Time in millis
const val PERMISSIONS_REQUEST_READ_PHONE_STATE = 1100

class MainActivity : AppCompatActivity() {

    private lateinit var appbarViewModel: AppbarViewModel
    private lateinit var nfcEventViewModel: NfcEventViewModel
    private lateinit var tripStudentViewModel: TripStatusViewModel
    private lateinit var mainViewModel: MainActivityViewModel

    private var nfcAdapter: NfcAdapter? = null
    private lateinit var nfcPendingIntent: PendingIntent
    private lateinit var nfcIntentFilters: Array<IntentFilter>
    private lateinit var nfcTechList: Array<Array<String>>

    private var isAutoPushEnabled: Boolean = false
    private val periodicTransactionsPushHandler = Handler()
    private val periodicPushRunnable = object: Runnable {
        override fun run() {
            if(isAutoPushEnabled) {
                mainViewModel.postCheckinTransactions(false)
                periodicTransactionsPushHandler.postDelayed(this, SYNC_FREQUENCY)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        Locale.getDefault().getLanguage()
        setupNfc()

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
            == PackageManager.PERMISSION_GRANTED) {
            getAndSaveImei()
        } else {
            ActivityCompat.requestPermissions(this,
                Array(1){Manifest.permission.READ_PHONE_STATE},
                PERMISSIONS_REQUEST_READ_PHONE_STATE)
        }

        val transactionsDao = CareTakerDatabase.getInstance(this).transactionsDao
        val mainActivityViewModelFactory = MainActivityViewModelFactory(transactionsDao, application)
        mainViewModel = ViewModelProviders.of(this, mainActivityViewModelFactory).get(MainActivityViewModel::class.java)
        nfcEventViewModel = ViewModelProviders.of(this).get(NfcEventViewModel::class.java)

        appbarViewModel = ViewModelProviders.of(this).get(AppbarViewModel::class.java)
        tripStudentViewModel = ViewModelProviders.of(this).get(TripStatusViewModel::class.java)
        appbarViewModel.appbarTitle.observe(this, Observer {
            toolbar.textAppbarTitle.text = it
        })
        appbarViewModel.appbarSubtitle.observe(this, Observer {
            toolbar.textAppbarSubtitle.text = it
        })
        appbarViewModel.appVersion.observe(this, Observer {
            toolbar.textVersion.text = it
        })

        buttonPendingTransaction.setOnClickListener {
            mainViewModel.postCheckinTransactions(true)
        }

        mainViewModel.dirtyCheckinTransactions.observe(this, Observer {
            Timber.d("Dirty Checkin Transactions (${it.size}): $it")
        })

        mainViewModel.dirtyTripEventTransactions.observe(this, Observer {
            Timber.d("Dirty Trip Event Transactions (${it.size}): $it")
        })

        mainViewModel.totalDirtyTransactions.observe(this, Observer { count ->
            if(count <= 0) {
                buttonPendingTransaction.visibility = View.INVISIBLE
            } else {
                buttonPendingTransaction.visibility = View.VISIBLE
            }
            buttonPendingTransaction.text = count.toString()
            mainViewModel.postCheckinTransactions(false)
        })

        mainViewModel.eventManualTransactionsPushFailed.observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })
    }

    @SuppressLint("MissingPermission", "HardwareIds")
    private fun getIMEINumber(): String? {
        val telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            telephonyManager.imei
        } else { // older OS  versions
            @Suppress("DEPRECATION")
            telephonyManager.deviceId
        }
    }

    private fun getAndSaveImei() {
        val imei = getIMEINumber()
        Timber.i("IMEI: $imei")
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString(PREF_KEY_IMEI_NUMBER, imei).apply()
    }

    private fun setupNfc() {
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)
        if(nfcAdapter == null) {
            Timber.w("This device does not support NFC")
            Snackbar.make(toolbar, "This device does not support NFC", Snackbar.LENGTH_SHORT).show()
        }

        handleIntent(intent)

        nfcPendingIntent = PendingIntent.getActivity(
            this, 0, Intent(this, javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0)
        val ndef = IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED)
        try {
            ndef.addDataType("*/*")    /* Handles all MIME based dispatches.
                                       You should specify only the ones that you need. */
        }
        catch (e: IntentFilter.MalformedMimeTypeException) {
            throw RuntimeException("fail", e)
        }
        nfcIntentFilters = arrayOf(ndef)
        nfcTechList = arrayOf(
            // All technologies
            arrayOf(IsoDep::class.java.name),
            arrayOf(NfcA::class.java.name),
            arrayOf(NfcB::class.java.name),
            arrayOf(NfcF::class.java.name),
            arrayOf(NfcV::class.java.name),
            arrayOf(Ndef::class.java.name),
            arrayOf(NdefFormatable::class.java.name),
            arrayOf(MifareClassic::class.java.name),
            arrayOf(MifareUltralight::class.java.name)
        )
    }

    private fun handleIntent(intent: Intent) {
        val action = intent.action
        Timber.d("Handle intent: $action")
        if(NfcAdapter.ACTION_TECH_DISCOVERED == action || NfcAdapter.ACTION_TAG_DISCOVERED == action) {
            val tagFromIntent: Tag? = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG)
            if(tagFromIntent != null) {
                val serialNumber = getHexDump(tagFromIntent.id)
                Timber.v("New NFC tag discovered, serial number: $serialNumber")
                nfcEventViewModel.nfcDiscovered(serialNumber)
            } else {
                Timber.e("NFC Tag was null")
            }
        }
    }

    private fun getHexDump(byteArray: ByteArray): String {
        var hexDump = ""
        for (aByte in byteArray) {
            val x = String.format("%02X", aByte)
            hexDump += x //+ ' ';
        }
        return hexDump.toUpperCase()
    }

    override fun onResume() {
        super.onResume()
        if(nfcAdapter?.isEnabled == false) {
            Timber.w("NFC is turned off")
            Snackbar.make(toolbar, "Please enable NFC", Snackbar.LENGTH_LONG).show()
        }
        nfcAdapter?.enableForegroundDispatch(this, nfcPendingIntent, nfcIntentFilters, nfcTechList)
    }

    override fun onPause() {
        super.onPause()
        nfcAdapter?.disableForegroundDispatch(this)
    }

    override fun onStart() {
        super.onStart()
        isAutoPushEnabled = true
        periodicTransactionsPushHandler.post(periodicPushRunnable)
    }

    override fun onStop() {
        super.onStop()
        isAutoPushEnabled = false
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        // This method gets called, when a new Intent gets associated with the current activity instance.
        // Instead of creating a new activity, onNewIntent will be called. For more information have a look
        // at the documentation.
        //
        // In our case this method gets called, when the user attaches a Tag to the device.
        //
        handleIntent(intent)
    }

    /*override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }*/

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            PERMISSIONS_REQUEST_READ_PHONE_STATE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getAndSaveImei()
                } else {
                    ActivityCompat.requestPermissions(this,
                        Array(1){Manifest.permission.READ_PHONE_STATE},
                        PERMISSIONS_REQUEST_READ_PHONE_STATE)
                }
            }
        }
    }

    override fun onBackPressed() {
        if(tripStudentViewModel.tripStatus.value == Trip.TripStatus.TRIP_IN_PROGRESS) {
            Timber.i("Trip in progress. Blocking onBackPressed()")
            // Snackbar.make(textAppbarSubtitle, "Trip in progress. Can't go back", Snackbar.LENGTH_SHORT).show()
            Toast.makeText(this, "Trip in progress. Can't go back", Toast.LENGTH_SHORT).show()
        } else {
            super.onBackPressed()
        }
    }
}
