package com.nvszones.caretaker

import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import com.crashlytics.android.Crashlytics
import com.facebook.drawee.backends.pipeline.Fresco
import com.nvszones.caretaker.utils.CrashReportingTree
import io.fabric.sdk.android.Fabric
import timber.log.Timber

class NVSCareTakerApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())
        if(BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(CrashReportingTree())
        }
        Fresco.initialize(this)
    }
    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LanguagePreference.setLocale(base))

    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        LanguagePreference.setLocale(this)
    }


}