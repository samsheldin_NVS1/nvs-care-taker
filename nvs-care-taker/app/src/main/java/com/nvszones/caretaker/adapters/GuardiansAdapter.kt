package com.nvszones.caretaker.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import com.nvszones.caretaker.R
import com.nvszones.caretaker.models.CheckinStatus
import com.nvszones.caretaker.models.Guardian
import com.nvszones.caretaker.network.BASE_URL

class GuardiansAdapter(): ListAdapter<Guardian, GuardiansAdapter.GuardianViewHolder>(GuardianDiffCallback()) {
    private var onClickListener: ((guardian: Guardian)->Unit)? = null

    var highlightCheckedIn = false
    var enableCheckin = true
    var showManualCheckinButton: Boolean = false

    fun setOnClickListener(listener: (guardian: Guardian) -> Unit) {
        this.onClickListener = listener
    }

    override fun onBindViewHolder(holder: GuardianViewHolder, position: Int) {
        holder.bind(getItem(position), showManualCheckinButton, enableCheckin, highlightCheckedIn, onClickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GuardianViewHolder {
        return GuardianViewHolder.from(parent)
    }

    companion object {
        fun isActive(item: Guardian): Boolean {
            return when(item.checkinStatus) {
                CheckinStatus.NOT_CHECKED_IN -> false
                CheckinStatus.CHECKED_IN -> true
            }
        }
    }

    class GuardianViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val textName = itemView.findViewById<TextView>(R.id.textGuardianName)
        private val imagePhoto = itemView.findViewById<SimpleDraweeView>(R.id.imageGuardianPhoto)
        private val textRelationship = itemView.findViewById<TextView>(R.id.textGuardianRelationship)
        private val buttonManualCheckin = itemView.findViewById<TextView>(R.id.buttonManuallyCheckinGuardian)

        fun bind(item: Guardian, showManualCheckinButton: Boolean, enableButtons: Boolean, highlightCheckedIn:Boolean, listener: ((guardian: Guardian) -> Unit)?) {
            textName.text = item.name
            if(item.photoUrl.isNullOrEmpty()) {
                imagePhoto.setActualImageResource(R.drawable.default_icon_guardian)
            } else {
                val photoUrl = BASE_URL + item.photoUrl
                imagePhoto.setImageURI(photoUrl)
            }
            imagePhoto.hierarchy.setFailureImage(R.drawable.default_icon_guardian)

            val isGreen = isActive(item)
            if(isGreen) {
                buttonManualCheckin.setText(R.string.checked_in)
                if(highlightCheckedIn) {
                    imagePhoto.setBackgroundResource(R.drawable.bg_student_item_green)
                }
            } else {
                buttonManualCheckin.setText(R.string.manually_checkin)
                imagePhoto.setBackgroundResource(R.drawable.bg_student_item_grey)
            }
            if(showManualCheckinButton) {
                buttonManualCheckin.visibility = View.VISIBLE
            } else {
                buttonManualCheckin.visibility = View.GONE
            }
            buttonManualCheckin.isEnabled = enableButtons

            textRelationship.text = item.relationship

            buttonManualCheckin.setOnClickListener {
                // Timber.i("Clicked on Student - ${item.name} (${item.id})")
                listener?.invoke(item)
            }
        }

        companion object {
            fun from(parent: ViewGroup): GuardianViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = layoutInflater.inflate(R.layout.layout_guardian_item, parent, false)
                return GuardianViewHolder(binding)
            }
        }
    }

    class GuardianDiffCallback: DiffUtil.ItemCallback<Guardian>() {
        override fun areItemsTheSame(oldItem: Guardian, newItem: Guardian): Boolean {
            // Timber.i("Same 1 item: ${oldItem.id == newItem.id}: [$oldItem]")
            // Timber.i("Same 1 item: ${oldItem.id == newItem.id}: [$newItem]")
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Guardian, newItem: Guardian): Boolean {
            // Timber.i("Same 1 content: ${oldItem == newItem}: [$oldItem]")
            // Timber.i("Same 1 content: ${oldItem == newItem}: [$newItem]")
            return oldItem == newItem
        }
    }
}