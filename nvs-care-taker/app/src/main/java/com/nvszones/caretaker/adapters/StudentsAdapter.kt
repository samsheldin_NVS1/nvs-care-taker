package com.nvszones.caretaker.adapters

import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.animation.doOnCancel
import androidx.core.animation.doOnEnd
import androidx.core.animation.doOnStart
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import com.nvszones.caretaker.R
import com.nvszones.caretaker.models.BoardingStatus
import com.nvszones.caretaker.models.Gender
import com.nvszones.caretaker.models.Student
import com.nvszones.caretaker.network.BASE_URL
import com.nvszones.caretaker.viewmodels.StudentViewModel
import com.nvszones.caretaker.viewmodels.StudentViewModel.BoardingFragmentMode

class StudentsAdapter(var items: List<Student>, private val mode: BoardingFragmentMode): RecyclerView.Adapter<StudentsAdapter.StudentViewHolder>() {

    private var onClickListener: ((student: Student)->Unit)? = null

    private var sortState: StudentViewModel.StudentListSortState = StudentViewModel.StudentListSortState.ALPHABETICAL

    fun setOnClickListener(listener: (student: Student) -> Unit) {
        this.onClickListener = listener
    }

    override fun onBindViewHolder(holder: StudentViewHolder, position: Int) {
        val current = items[position]
        holder.bind(current, onClickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentViewHolder {
        return StudentViewHolder.from(parent, mode)
    }

    fun setSortState(sortState: StudentViewModel.StudentListSortState) {
        this.sortState = sortState
        this.submitList(items)
    }

    override fun getItemId(position: Int): Long {
        return items[position].id
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = items.size

    companion object {
        fun isActive(item: Student, mode: BoardingFragmentMode): Boolean {
            return when (mode) {
                BoardingFragmentMode.MODE_PICKUP_BOARDING, BoardingFragmentMode.MODE_GATE_BOARDING -> item.boardingStatus == BoardingStatus.BOARDED || item.boardingStatus == BoardingStatus.SCHEDULED_TO_OFF_BOARD || item.boardingStatus == BoardingStatus.OFF_BOARDED
                BoardingFragmentMode.MODE_DROP_BOARDING -> item.boardingStatus == BoardingStatus.SCHEDULED_TO_OFF_BOARD || item.boardingStatus == BoardingStatus.OFF_BOARDED
                BoardingFragmentMode.MODE_DROP_OFF, BoardingFragmentMode.MODE_GATE_DROP_OFF -> item.boardingStatus == BoardingStatus.OFF_BOARDED
            }
        }
    }

    class StudentViewHolder(itemView: View, private val mode: BoardingFragmentMode): RecyclerView.ViewHolder(itemView) {
        private val textName = itemView.findViewById<TextView>(R.id.textStudentName)
        private val imagePhoto = itemView.findViewById<SimpleDraweeView>(R.id.imageStudentPhoto)
        private val imageStudentSecureIndicator = itemView.findViewById<ImageView>(R.id.imageStudentSecureIndicator)
        val imageCheckInIcon = itemView.findViewById<ImageView>(R.id.imageCheckInIndicator)
        private val imageCrossBoardIcon = itemView.findViewById<ImageView>(R.id.imageCrossBoardIndicator)

        fun bind(item: Student, listener: ((student: Student) -> Unit)?) {
            textName.text = item.name
            if(item.photoUrl.isNullOrEmpty()) {
                // TODO: Get proper svg images to simplify this flow. Add failure and loading image resources
                if(item.gender == Gender.MALE) {
                    // TODO: This is a male photo
                    imagePhoto.setActualImageResource(R.drawable.default_icon_girl)
                } else {
                    imagePhoto.setActualImageResource(R.drawable.default_icon_girl)
                }
            } else {
                val photoUrl = BASE_URL + item.photoUrl
                imagePhoto.setImageURI(photoUrl)
            }

            if(item.gender == Gender.MALE) {
                // TODO: This is a male photo
                imagePhoto.hierarchy.setFailureImage(R.drawable.default_icon_girl)
            } else {
                imagePhoto.hierarchy.setFailureImage(R.drawable.default_icon_girl)
            }
            val isGreen = isActive(item, mode)
            if(isGreen) {
                imagePhoto.setBackgroundResource(R.drawable.bg_student_item_green)
                imageCheckInIcon.visibility = View.VISIBLE
            } else {
                imagePhoto.setBackgroundResource(R.drawable.bg_student_item_grey)
                imageCheckInIcon.visibility = View.GONE
            }
            imageStudentSecureIndicator.visibility = if(item.selfDropEnabled) View.GONE else View.VISIBLE
            imageCrossBoardIcon.visibility = if(item.isCrossBoarded) View.VISIBLE else View.GONE

            itemView.setOnClickListener {
                // Timber.i("Clicked on Student - ${item.name} (${item.id})")
                listener?.invoke(item)
            }
        }

        companion object {
            fun from(parent: ViewGroup, mode: BoardingFragmentMode): StudentViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = layoutInflater.inflate(R.layout.layout_student_item, parent, false)
                return StudentViewHolder(binding, mode)
            }
        }
    }

    fun submitList(newItems: List<Student>) {
        // Deep clone the newItems into another list because list assignment will add objects by reference and diff utils
        // comparison will never detect changes. Deep clone can solve the problem
        val newItemsImpl = mutableListOf<Student>()
        newItems.forEach {
            newItemsImpl.add(it.copy())
        }
        val sortedList = if(sortState == StudentViewModel.StudentListSortState.ALPHABETICAL) {
            newItemsImpl.sortedWith(compareBy (String.CASE_INSENSITIVE_ORDER, { it.name }))
        } else {
            newItemsImpl.sortedWith(compareByDescending<Student> { isActive(it, mode) }.thenBy (String.CASE_INSENSITIVE_ORDER, { it.name }))
        }
        // Timber.v("Submit list old: $sortState, $sortedList")
        // Timber.v("Submit list new: $sortState, ${this.items}")
        val diffUtils = DiffUtil.calculateDiff(StudentDiffCallback(this.items, sortedList))
        this.items = sortedList
        diffUtils.dispatchUpdatesTo(this)
    }

    /*
    class StudentDiffCallback: DiffUtil.ItemCallback<Student>() {
        override fun areItemsTheSame(oldItem: Student, newItem: Student): Boolean {
            // Timber.i("Same 1 item: ${oldItem.id == newItem.id}: [$oldItem]")
            // Timber.i("Same 1 item: ${oldItem.id == newItem.id}: [$newItem]")
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Student, newItem: Student): Boolean {
            // Timber.i("Same 1 content: ${oldItem == newItem}: [$oldItem]")
            // Timber.i("Same 1 content: ${oldItem == newItem}: [$newItem]")
            return oldItem == newItem
        }
    }*/

    class StudentDiffCallback(val oldItems: List<Student>, val newItems: List<Student>): DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldItems[oldItemPosition].id == newItems[newItemPosition].id
        }

        override fun getOldListSize(): Int {
            return oldItems.size
        }

        override fun getNewListSize(): Int {
            return newItems.size
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            // Timber.v("Submit Comparing ${oldItems[oldItemPosition] == newItems[newItemPosition]}: ${oldItems[oldItemPosition]} with ${newItems[newItemPosition]}")
            return oldItems[oldItemPosition] == newItems[newItemPosition]
        }

    }

    class StudentUpdatedAnimator: DefaultItemAnimator() {

        private var isRunning = false
        private val animatorMap = mutableMapOf<RecyclerView.ViewHolder, ObjectAnimator>() // Cache

        override fun animateChange(
            oldHolder: RecyclerView.ViewHolder,
            newHolder: RecyclerView.ViewHolder,
            preInfo: ItemHolderInfo,
            postInfo: ItemHolderInfo
        ): Boolean {
            val checkedIndicator = (newHolder as StudentViewHolder).imageCheckInIcon
            checkedIndicator.clearAnimation()
            isRunning = false

            if(checkedIndicator.visibility == View.VISIBLE) { // Apply pulse animation on checkbox only if it is visible
                if(animatorMap.containsKey(newHolder)) { // If the animation is already present, reuse
                    val anim = animatorMap[newHolder]
                    if(anim?.isRunning == true) {
                        anim.cancel() // Cancel any ongoing animation
                    }
                    anim?.start()
                } else { // Otherwise, create and add it to cache
                    val scaleDownAnimation = ObjectAnimator.ofPropertyValuesHolder(
                        checkedIndicator,
                        PropertyValuesHolder.ofFloat("scaleX", 1.4f),
                        PropertyValuesHolder.ofFloat("scaleY", 1.4f)
                    )
                    scaleDownAnimation.duration = 350
                    scaleDownAnimation.repeatCount = 5 // Should be an odd number to come back to original state
                    scaleDownAnimation.repeatMode = ObjectAnimator.REVERSE
                    scaleDownAnimation.doOnEnd {
                        isRunning = false
                        checkedIndicator.clearColorFilter()
                        dispatchAddFinished(newHolder)
                    }
                    scaleDownAnimation.doOnStart {
                        isRunning = true
                        checkedIndicator.setColorFilter(
                            ContextCompat.getColor(
                                checkedIndicator.context,
                                R.color.md_green_500
                            )
                        )
                    }
                    scaleDownAnimation.doOnCancel {
                        isRunning = false
                        // Timber.i("Cancelled anim")
                    }
                    animatorMap[newHolder] = scaleDownAnimation
                    scaleDownAnimation.start()
                }

            }
            return false
            // return super.animateChange(oldHolder, newHolder, preInfo, postInfo)
        }

        override fun canReuseUpdatedViewHolder(viewHolder: RecyclerView.ViewHolder): Boolean {
            return true
        }

        override fun isRunning(): Boolean {
            return isRunning
        }
    }
}

// Extension function to implement smooth scrolling on recycler view
fun RecyclerView.smoothSnapToPosition(position: Int, snapMode: Int = LinearSmoothScroller.SNAP_TO_START) {
    val smoothScroller = object: LinearSmoothScroller(this.context) {
        override fun getVerticalSnapPreference(): Int {
            return snapMode
        }

        override fun getHorizontalSnapPreference(): Int {
            return snapMode
        }
    }
    smoothScroller.targetPosition = position
    layoutManager?.startSmoothScroll(smoothScroller)
}