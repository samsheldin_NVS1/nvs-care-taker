package com.nvszones.caretaker.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nvszones.caretaker.R
import com.nvszones.caretaker.models.Trip

class TripsAdapter: ListAdapter<Trip, TripsAdapter.TripViewHolder>(TripDiffCallback()) {

    private var onBoardingClickListener: ((trip: Trip) -> Unit)? = null
    private var onDropBoardingClickListener: ((trip: Trip) -> Unit)? = null
    private var onDropoffClickListener: ((trip: Trip) -> Unit)? = null

    fun setOnBoardingClickListener(listener: (trip: Trip) -> Unit) {
        this.onBoardingClickListener = listener
    }

    fun setOnDropBoardingClickListener(listener: (trip: Trip) -> Unit) {
        this.onDropBoardingClickListener = listener
    }

    fun setOnDropOffClickListener(listener: (trip: Trip) -> Unit) {
        this.onDropoffClickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TripViewHolder {
        return TripViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: TripViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, onBoardingClickListener, onDropBoardingClickListener, onDropoffClickListener)
    }

    class TripViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val textFriendlyName = itemView.findViewById<TextView>(R.id.textFriendlyTripName)
        private val textName = itemView.findViewById<TextView>(R.id.textTripName)
        private val textTimings = itemView.findViewById<TextView>(R.id.textTripTimings)
        private val imagePickupBoarding = itemView.findViewById<ImageView>(R.id.imagePickupBoarding)
        private val imageDropBoarding = itemView.findViewById<ImageView>(R.id.imageDropBoarding)
        private val imageDropOffboarding = itemView.findViewById<ImageView>(R.id.imageDropOffboarding)

        fun bind(item: Trip, onBoardingClickListener: ((trip: Trip)->Unit)?, onDropBoardingClickListener: ((trip: Trip)->Unit)?, onDropoffClickListener: ((trip: Trip)->Unit)?) {
            textFriendlyName.text = item.tripFriendlyText
            textName.text = item.tripName
            textTimings.text = itemView.resources.getString(R.string.trip_timings_text, item.getFriendlyStartTime(), item.getFriendlyEndTime())

            when(item.tripType) {
                Trip.TripType.TRIP_TYPE_PICKUP -> {
                    imagePickupBoarding.visibility = View.VISIBLE
                    imageDropBoarding.visibility = View.INVISIBLE
                    imageDropOffboarding.visibility = View.INVISIBLE
                }

                Trip.TripType.TRIP_TYPE_DROP -> {
                    imagePickupBoarding.visibility = View.INVISIBLE
                    imageDropBoarding.visibility = View.VISIBLE
                    imageDropOffboarding.visibility = View.VISIBLE
                }

                Trip.TripType.TRIP_TYPE_GATE_ENTRY -> {
                    imagePickupBoarding.visibility = View.VISIBLE
                    imageDropBoarding.visibility = View.INVISIBLE
                    imageDropOffboarding.visibility = View.INVISIBLE
                }

                Trip.TripType.TRIP_TYPE_GATE_EXIT -> {
                    imagePickupBoarding.visibility = View.INVISIBLE
                    imageDropBoarding.visibility = View.INVISIBLE
                    imageDropOffboarding.visibility = View.VISIBLE
                }

                else -> {
                    imagePickupBoarding.visibility = View.INVISIBLE
                    imageDropBoarding.visibility = View.INVISIBLE
                    imageDropOffboarding.visibility = View.INVISIBLE
                }
            }

            /* old code: if(item.tripType == Trip.TRIP_TYPE_PICKUP) {
                imagePickupBoarding.visibility = View.VISIBLE
                imageDropBoarding.visibility = View.INVISIBLE
                imageDropOffboarding.visibility = View.INVISIBLE
            } else {
                imagePickupBoarding.visibility = View.INVISIBLE
                imageDropBoarding.visibility = View.VISIBLE
                imageDropOffboarding.visibility = View.VISIBLE
            } */

            imagePickupBoarding.setOnClickListener {
                onBoardingClickListener?.invoke(item)
                // Timber.i("Pickup boarding clicked")
                // it.findNavController().navigate(R.id.action_tripsFragment_to_pickupBoardingFragment)
            }

            imageDropBoarding.setOnClickListener {
                onDropBoardingClickListener?.invoke(item)
                // Timber.i("Drop boarding clicked")
            }

            imageDropOffboarding.setOnClickListener {
                onDropoffClickListener?.invoke(item)
                // Timber.i("Drop offboarding clicked")
            }
        }

        companion object {
            fun from(parent: ViewGroup): TripViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = layoutInflater.inflate(R.layout.layout_trip_item, parent, false)
                return TripViewHolder(binding)
            }
        }
    }

    class TripDiffCallback : DiffUtil.ItemCallback<Trip>() {
        override fun areItemsTheSame(oldItem: Trip, newItem: Trip): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Trip, newItem: Trip): Boolean {
            return oldItem == newItem
        }
    }
}