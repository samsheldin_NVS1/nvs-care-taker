package com.nvszones.caretaker.dao

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.nvszones.caretaker.models.*

@Database(entities = [Trip::class, BaseStudent::class, BaseGuardian::class, StudentTripRelationship::class, StudentGuardianRelationship::class, CheckinTransaction::class, TripEventTransaction::class], version = 1, exportSchema = false)
@TypeConverters(Converter::class)
abstract class CareTakerDatabase: RoomDatabase() {
    abstract val tripsDao: TripsDao
    abstract val studentsDao: StudentsDao
    abstract val guardiansDao: GuardiansDao
    abstract val transactionsDao: TransactionsDao

    companion object {
        @Volatile
        private var INSTANCE: CareTakerDatabase? = null

        fun getInstance(context: Context): CareTakerDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if(instance == null) {
                    instance = Room.databaseBuilder(context.applicationContext, CareTakerDatabase::class.java, "care_taker_database")
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}