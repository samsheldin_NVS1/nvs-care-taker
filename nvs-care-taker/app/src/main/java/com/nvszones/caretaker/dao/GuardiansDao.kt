package com.nvszones.caretaker.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.nvszones.caretaker.models.BaseGuardian
import com.nvszones.caretaker.models.CheckinStatus
import com.nvszones.caretaker.models.Guardian
import com.nvszones.caretaker.models.StudentGuardianRelationship

@Dao
interface GuardiansDao {
    @Query("SELECT * FROM guardians")
    fun getAllBaseGuardians(): LiveData<List<BaseGuardian>>

    @Query("SELECT guardians.id, guardians.name, guardians.gender, student_guardian_relationship.relationship, guardians.cardId, guardians.photoUrl, student_guardian_relationship.isPrimaryGuardian, guardians.phoneNumber,  student_guardian_relationship.checkinStatus, student_guardian_relationship.isManualCheckin FROM guardians INNER JOIN student_guardian_relationship ON guardians.id = student_guardian_relationship.guardianId AND studentId = :studentId")
    //@Query("SELECT studentId FROM student_trip_relationship WHERE tripId = :tripId")
    //@Query("SELECT students.id, students.name, students.studentClass, students.gender, students.cardId, students.photoUrl, students.selfDropEnabled FROM students INNER JOIN student_trip_relationship R ON students.id = R.studentId")
    fun getAllGuardians(studentId: Long): LiveData<List<Guardian>>

    @Query("SELECT guardians.id, guardians.name, guardians.gender, student_guardian_relationship.relationship, guardians.cardId, guardians.photoUrl, student_guardian_relationship.isPrimaryGuardian, guardians.phoneNumber,  student_guardian_relationship.checkinStatus, student_guardian_relationship.isManualCheckin FROM guardians INNER JOIN student_guardian_relationship ON guardians.id = student_guardian_relationship.guardianId AND studentId = :studentId AND checkinStatus = 1")
    fun getAllCheckedInGuardians(studentId: Long): List<Guardian>

    @Query("SELECT guardians.id, guardians.name, guardians.gender, student_guardian_relationship.relationship, guardians.cardId, guardians.photoUrl, student_guardian_relationship.isPrimaryGuardian, guardians.phoneNumber,  student_guardian_relationship.checkinStatus, student_guardian_relationship.isManualCheckin FROM guardians INNER JOIN student_guardian_relationship ON guardians.id = student_guardian_relationship.guardianId AND studentId = :studentId AND isPrimaryGuardian = 1 LIMIT 1")
    fun getPrimaryGuardian(studentId: Long): LiveData<Guardian>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(baseGuardian: BaseGuardian): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    fun insert(baseGuardians: List<BaseGuardian>): List<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(studentGuardianRelationship: StudentGuardianRelationship)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRelationships(relationships: List<StudentGuardianRelationship>): List<Long>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(baseGuardian: BaseGuardian)

    @Transaction
    fun insert(guardians: List<BaseGuardian>, studentGuardianRelationship: List<StudentGuardianRelationship>) {
        insert(guardians)
        insertRelationships(studentGuardianRelationship)
    }

    @Query("UPDATE student_guardian_relationship SET checkinStatus = :checkinStatus, isManualCheckin = :isManual WHERE studentId = :studentId AND guardianId = :guardianId")
    fun updateCheckinStatus(studentId: Long, guardianId: Long, checkinStatus: CheckinStatus, isManual: Boolean): Int

    @Query("SELECT * FROM guardians WHERE id = :key LIMIT 1")
    fun get(key: Long): BaseGuardian

    @Query("DELETE FROM guardians")
    fun clearGuardians()

    @Query("DELETE FROM student_guardian_relationship")
    fun clearRelationships()

    @Transaction
    fun clear() {
        clearRelationships()
        clearGuardians()
    }
}