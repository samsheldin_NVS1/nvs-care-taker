package com.nvszones.caretaker.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.nvszones.caretaker.models.BaseStudent
import com.nvszones.caretaker.models.BoardingStatus
import com.nvszones.caretaker.models.Student
import com.nvszones.caretaker.models.StudentTripRelationship

@Dao
interface StudentsDao {
    @Query("SELECT * FROM students")
    fun getAllStudents(): LiveData<List<BaseStudent>>

    @Query("SELECT students.id, students.name, students.studentClass, students.gender, students.cardId, students.photoUrl, students.selfDropEnabled, student_trip_relationship.boardingStatus, student_trip_relationship.isManualCheckin, isCrossBoarded FROM students INNER JOIN student_trip_relationship ON students.id = student_trip_relationship.studentId AND tripId = :tripId")
    //@Query("SELECT studentId FROM student_trip_relationship WHERE tripId = :tripId")
    //@Query("SELECT students.id, students.name, students.studentClass, students.gender, students.cardId, students.photoUrl, students.selfDropEnabled FROM students INNER JOIN student_trip_relationship R ON students.id = R.studentId")
    fun getAllStudents(tripId: Long): LiveData<List<Student>>

    // Refer Student.Converters for boarding status integer constants
    // @Query("SELECT * FROM students WHERE tripId = :tripId AND (boardingStatus = 0 OR boardingStatus = 1 OR boardingStatus = 2 OR boardingStatus = 3)")
    //@Query("SELECT * FROM students")
    //fun getPickupBoardingStudents(tripId: Long): LiveData<List<BaseStudent>>

    // @Query("SELECT * FROM students WHERE tripId = :tripId AND (boardingStatus = 1 OR boardingStatus = 2 OR boardingStatus = 3)")
    //@Query("SELECT * FROM students")
    //fun getDropboardingStudents(tripId: Long): LiveData<List<BaseStudent>>

    // @Query("SELECT * FROM students WHERE tripId = :tripId AND (boardingStatus = 2 OR boardingStatus = 3)")
    //@Query("SELECT * FROM students")
    @Query("SELECT students.id, students.name, students.studentClass, students.gender, students.cardId, students.photoUrl, students.selfDropEnabled, student_trip_relationship.boardingStatus, student_trip_relationship.isManualCheckin, isCrossBoarded FROM students INNER JOIN student_trip_relationship ON students.id = student_trip_relationship.studentId AND tripId = :tripId AND (boardingStatus = 2 OR boardingStatus = 3)")
    fun getDropOffStudents(tripId: Long): LiveData<List<Student>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(student: BaseStudent): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(studentTripRelationship: StudentTripRelationship)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    fun insert(students: List<BaseStudent>): List<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRelationships(relationships: List<StudentTripRelationship>): List<Long>

    @Transaction
    fun insert(students: List<BaseStudent>, studentTripRelationships: List<StudentTripRelationship>) {
        insert(students)
        insertRelationships(studentTripRelationships)
    }

    @Query("UPDATE student_trip_relationship SET boardingStatus = :boardingStatus, isManualCheckin = :isManual, isCrossBoarded = :isCrossBoarded WHERE studentId = :studentId AND tripId = :tripId")
    fun updateBoardingStatus(studentId: Long, tripId: Long, boardingStatus: BoardingStatus, isManual: Boolean, isCrossBoarded: Boolean): Int

    @Query("SELECT * FROM students WHERE id = :key LIMIT 1")
    fun get(key: Long): LiveData<BaseStudent>

    @Query("SELECT * FROM students WHERE cardId = :cardId LIMIT 1")
    fun get(cardId: String): BaseStudent?

    @Query("SELECT students.id, students.name, students.studentClass, students.gender, students.cardId, students.photoUrl, students.selfDropEnabled, student_trip_relationship.boardingStatus, student_trip_relationship.isManualCheckin, isCrossBoarded FROM students INNER JOIN student_trip_relationship ON students.id = student_trip_relationship.studentId AND tripId = :tripId AND studentId = :studentId LIMIT 1")
    fun get(studentId: Long, tripId: Long): LiveData<Student>

    @Query("SELECT students.id, students.name, students.studentClass, students.gender, students.cardId, students.photoUrl, students.selfDropEnabled, student_trip_relationship.boardingStatus, student_trip_relationship.isManualCheckin, isCrossBoarded FROM students INNER JOIN student_trip_relationship ON students.id = student_trip_relationship.studentId AND tripId = :tripId AND studentId = :studentId LIMIT 1")
    fun getStudent(studentId: Long, tripId: Long): Student?

    @Query("DELETE FROM student_trip_relationship WHERE studentId = :studentId AND tripId = :tripId")
    fun deleteStudentTripRelationShip(studentId: Long, tripId: Long): Int

    @Query("DELETE FROM students")
    fun clearStudents()

    @Query("DELETE FROM student_trip_relationship")
    fun clearRelationships()

    @Transaction
    fun clear() {
        clearRelationships()
        clearStudents()
    }
}