package com.nvszones.caretaker.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.nvszones.caretaker.models.CheckinTransaction
import com.nvszones.caretaker.models.TripEventTransaction

@Dao
interface TransactionsDao {
    @Query("SELECT * FROM checkin_transactions")
    fun getAllTransactions(): LiveData<List<CheckinTransaction>>

    @Query("SELECT * FROM checkin_transactions WHERE dirty = 1")
    fun getAllDirtyCheckinTransactions(): LiveData<List<CheckinTransaction>>

    @Query("SELECT * FROM trip_event_transactions WHERE dirty = 1")
    fun getAllDirtyTripEventTransactions(): LiveData<List<TripEventTransaction>>

    @Query("UPDATE checkin_transactions SET dirty=0 WHERE dirty=1 AND id IN (:ids)")
    fun updateDirtyCheckinTransactions(ids: List<Long>): Int

    @Query("UPDATE trip_event_transactions SET dirty=0 WHERE dirty=1 AND id IN (:ids)")
    fun updateDirtyTripEventTransactions(ids: List<Long>): Int

    @Insert
    fun insert(transaction: CheckinTransaction): Long

    @Insert
    fun insert(tripEventTransaction: TripEventTransaction): Long

    // @Query("UPDATE checkin_transactions SET dirty = :dirty WHERE id = :transactionId")
    // fun updateTransaction(transactionId: Long, dirty: Boolean): Int

    @Query("SELECT * FROM checkin_transactions WHERE id = :key LIMIT 1")
    fun get(key: Long): LiveData<CheckinTransaction>

    @Query("DELETE FROM checkin_transactions")
    fun clearCheckinTransactions()

    @Query("DELETE FROM trip_event_transactions")
    fun clearTripEventTransactions()

    @Transaction
    fun clear() {
        clearCheckinTransactions()
        clearTripEventTransactions()
    }

}