package com.nvszones.caretaker.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.nvszones.caretaker.models.Trip

@Dao
interface TripsDao {
    @Query("SELECT * FROM trips WHERE routeId = :routeId")
    fun getAllTrips(routeId: Long): LiveData<List<Trip>>

    @Query("SELECT * FROM trips")
    fun getAllTrips(): LiveData<List<Trip>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(trip: Trip): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    fun insert(trips: List<Trip>): List<Long>

    @Query("SELECT * FROM trips WHERE id = :key LIMIT 1")
    fun get(key: Long): LiveData<Trip>

    @Query("DELETE FROM trips")
    fun clear()
}