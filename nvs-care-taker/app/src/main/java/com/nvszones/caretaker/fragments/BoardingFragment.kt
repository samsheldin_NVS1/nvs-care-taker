package com.nvszones.caretaker.fragments


import android.Manifest
import android.content.IntentSender
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.material.snackbar.Snackbar
import com.nvszones.caretaker.R
import com.nvszones.caretaker.adapters.StudentsAdapter
import com.nvszones.caretaker.adapters.smoothSnapToPosition
import com.nvszones.caretaker.dao.CareTakerDatabase
import com.nvszones.caretaker.models.Student
import com.nvszones.caretaker.models.Trip
import com.nvszones.caretaker.utils.putEnum
import com.nvszones.caretaker.viewmodels.NfcEventViewModel
import com.nvszones.caretaker.viewmodels.StudentViewModel
import com.nvszones.caretaker.viewmodels.StudentsViewModelFactory
import com.nvszones.caretaker.viewmodels.TripStatusViewModel
import kotlinx.android.synthetic.main.fragment_boarding.*
import kotlinx.android.synthetic.main.layout_collapsible_trip_summary.*
import timber.log.Timber

/**
 * A simple [Fragment] subclass.
 *
 */
class BoardingFragment : Fragment() {

    private val args: BoardingFragmentArgs by navArgs()

    private var tripId: Long = -1
    private lateinit var mode: StudentViewModel.BoardingFragmentMode

    private lateinit var adapter: StudentsAdapter
    private lateinit var studentViewModel: StudentViewModel
    private lateinit var gridLayoutManager: GridLayoutManager

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback
    private var requestingLocationUpdates: Boolean = false
    private lateinit var locationRequest: LocationRequest

    private val REQUESTING_LOCATION_UPDATES_KEY = "requestingLocationUpdates"
    private val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1000
    private val FASTEST_INTERVAL = 5000L
    private val UPDATE_INTERVAL = 30 * 1000L

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        updateValuesFromBundle(savedInstanceState)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_boarding, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tripId = args.tripId
        mode = args.mode
        val application = requireNotNull(activity).application
        val database = CareTakerDatabase.getInstance(application)

        val studentViewModelFactory = StudentsViewModelFactory(mode, tripId, database, application)
        studentViewModel = ViewModelProviders.of(this, studentViewModelFactory).get(StudentViewModel::class.java)
        val tripStudentViewModel = ViewModelProviders.of(requireNotNull(activity)).get(TripStatusViewModel::class.java)
        val nfcEventViewModel = ViewModelProviders.of(requireNotNull(activity)).get(NfcEventViewModel::class.java)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireNotNull(activity))
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                studentViewModel.lastKnownLocation = locationResult.lastLocation
                Timber.v("Location: ${locationResult.lastLocation.latitude} ${locationResult.lastLocation.longitude}, ${locationResult.lastLocation.time}")
            }
        }
        locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = UPDATE_INTERVAL
        locationRequest.fastestInterval = FASTEST_INTERVAL
        getLocationPermission()

        adapter = StudentsAdapter(ArrayList<Student>(), mode)
        gridLayoutManager = GridLayoutManager(view.context, 3)
        adapter.setHasStableIds(true)
        recyclerStudentList.itemAnimator = StudentsAdapter.StudentUpdatedAnimator()
        recyclerStudentList.adapter = adapter
        recyclerStudentList.layoutManager = gridLayoutManager
        recyclerStudentList.visibility = View.GONE
        textNoStudentsToShow.visibility = View.VISIBLE
        studentViewModel.students.observe(this, Observer {
            Timber.v("Students: ${it.size}: $it")
            /*if(adapter.itemCount == 0) {
                Timber.w("Submitting list")
                adapter.submitList(it)
            }*/
            // adapter.submitList(null)
            // adapter.submitList(it)
            /* Updating with submit list
            adapter.setItems(it)
            adapter.notifyDataSetChanged() */
            adapter.submitList(it)

            textTotal.text = resources.getString(R.string.boarding_total_summary_placeholder, studentViewModel.countBoarded, studentViewModel.countScheduled)
            textCrossBoarded.text = studentViewModel.countCrossBoarded.toString()
            if(studentViewModel.countCrossBoarded <= 0 || mode == StudentViewModel.BoardingFragmentMode.MODE_DROP_OFF || mode == StudentViewModel.BoardingFragmentMode.MODE_GATE_DROP_OFF) {
                textPlus.visibility = View.GONE
                textCrossBoarded.visibility = View.GONE
            } else {
                textPlus.visibility = View.VISIBLE
                textCrossBoarded.visibility = View.VISIBLE
            }

            if(it == null || it.isEmpty()) {
                recyclerStudentList.visibility = View.GONE
                textNoStudentsToShow.visibility = View.VISIBLE
                buttonStartStopTrip.visibility = View.GONE
            } else {
                recyclerStudentList.visibility = View.VISIBLE
                textNoStudentsToShow.visibility = View.GONE
                buttonStartStopTrip.visibility = View.VISIBLE
            }
        })

        buttonStartStopTrip.setOnClickListener {
            if(studentViewModel.tripStatus.value == Trip.TripStatus.TRIP_IN_PROGRESS) {
                val builder = AlertDialog.Builder(this.requireContext())
                val dialogView: View = layoutInflater.inflate(R.layout.layout_trip_summary_dialog, null)

                /*val countScheduled = studentViewModel.students.value?.count { student -> !student.isCrossBoarded } ?: 0
                val countBoarded = studentViewModel.students.value?.count { student -> StudentsAdapter.isActive(student, mode) && !student.isCrossBoarded } ?: 0
                val countMissed = studentViewModel.students.value?.count { student -> !StudentsAdapter.isActive(student, mode) && !student.isCrossBoarded } ?: 0
                val countCrossBoarded = studentViewModel.students.value?.count { student -> StudentsAdapter.isActive(student, mode) && student.isCrossBoarded } ?: 0*/

                if(mode == StudentViewModel.BoardingFragmentMode.MODE_DROP_OFF || mode == StudentViewModel.BoardingFragmentMode.MODE_GATE_DROP_OFF) {
                    dialogView.findViewById<TextView>(R.id.textCountCrossBoarded).visibility = View.GONE
                    dialogView.findViewById<TextView>(R.id.labelCrossBoarded).visibility = View.GONE
                } else {
                    dialogView.findViewById<TextView>(R.id.textCountCrossBoarded).visibility = View.VISIBLE
                    dialogView.findViewById<TextView>(R.id.labelCrossBoarded).visibility = View.VISIBLE
                }
                dialogView.findViewById<TextView>(R.id.textCountScheduled).text = studentViewModel.countScheduled.toString()
                dialogView.findViewById<TextView>(R.id.textCountBoarded).text = studentViewModel.countBoarded.toString()
                dialogView.findViewById<TextView>(R.id.textCountMissed).text = studentViewModel.countMissed.toString()
                dialogView.findViewById<TextView>(R.id.textCountCrossBoarded).text = studentViewModel.countCrossBoarded.toString()
                dialogView.findViewById<TextView>(R.id.textCountTotal).text = (studentViewModel.countBoarded + studentViewModel.countCrossBoarded).toString()
                builder.setView(dialogView)
                builder.setPositiveButton("End Trip") { _, _ ->
                    studentViewModel.onTripStartStopClick()
                    findNavController().navigateUp()
                }
                builder.setNegativeButton(R.string.cancel, null)
                builder.setTitle("Do you want to end trip?")
                builder.show()
            } else {
                studentViewModel.onTripStartStopClick()
            }
        }

        buttonSort.setOnClickListener {
            studentViewModel.toggleSort()
        }

        studentViewModel.sortState.observe(this, Observer {
            adapter.setSortState(it)
            gridLayoutManager.scrollToPosition(0)
            // recyclerStudentList.smoothScrollToPosition(0)
            if(it == StudentViewModel.StudentListSortState.ALPHABETICAL) {
                buttonSort.alpha = 1.0f
            } else {
                buttonSort.alpha = 0.6f
            }
        })

        studentViewModel.currentTrip.observe(this, Observer {
            if(it.isStartTripAllowed()) {
                buttonStartStopTrip.setBackgroundResource(R.color.colorPrimaryDark)
                // buttonStartStopTrip.isEnabled = true
            } else {
                buttonStartStopTrip.setBackgroundResource(R.color.md_red_200)
                // buttonStartStopTrip.isEnabled = false
            }
        })

        studentViewModel.tripStatus.observe(this, Observer { tripStatus->
            tripStudentViewModel.tripStatus.value = tripStatus
            run {
                if (tripStatus == Trip.TripStatus.TRIP_IN_PROGRESS) {
                    buttonStartStopTrip.text = getString(R.string.end_trip)
                    buttonStartStopTrip.setBackgroundResource(R.color.md_red_300)
                } else {
                    buttonStartStopTrip.text = getString(R.string.start_trip)
                    // Timber.i("Start trip allowed: ${studentViewModel.currentTrip.value?.isStartTripAllowed()}")
                    if(studentViewModel.currentTrip.value?.isStartTripAllowed() == true) {
                        buttonStartStopTrip.setBackgroundResource(R.color.colorPrimaryDark)
                    } else {
                        buttonStartStopTrip.setBackgroundResource(R.color.md_red_200)
                    }
                }
            }
        })

        studentViewModel.eventErrorStartingStoppingTrip.observe(this, Observer {
            Snackbar.make(buttonStartStopTrip, it, Snackbar.LENGTH_SHORT).show()
        })

        studentViewModel.eventTripNotStarted.observe(this, Observer {
            Snackbar.make(recyclerStudentList, "Trip not started", Snackbar.LENGTH_SHORT).show()
        })

        studentViewModel.currentTrip.observe(this, Observer {
            textRouteName.text = it.tripName
            textRouteNumber.text = it.tripFriendlyText
            textRouteTimings.text = resources.getString(R.string.boarding_time_summary_placeholder, it.getFriendlyStartTime(), it.getFriendlyEndTime())
        })

        studentViewModel.eventErrorUpdatingStudent.observe(this, Observer {
            // TODO: Custom toast
            Toast.makeText(activity, it, Toast.LENGTH_SHORT).show()
        })

        adapter.setOnClickListener {
            Timber.i("Clicked: $it")
            // studentViewModel.onStudentClick(it)
            showDetailsFragment(it, false)
        }

        nfcEventViewModel.eventNfcDiscovered.observe(this, Observer {
            if(!StudentDetailsFragment.getInstance().isAdded) {
                Timber.i("Handling NFC in boarding fragment")
                studentViewModel.onStudentCardDiscovered(it)
            }
        })

        studentViewModel.eventStudentUpdated.observe(this, Observer {updatedStudent ->
            val items = studentViewModel.students.value
            /* Updating with submit list
            if(items != null) {
                adapter.setItems(items)
            }
            adapter.notifyDataSetChanged()
            */
            if(items != null) {
                adapter.submitList(items)
                val position = adapter.items.indexOfFirst { student -> student.id == updatedStudent.id }
                recyclerStudentList.smoothSnapToPosition(position)
                Handler().postDelayed({
                    if(!recyclerStudentList.isAnimating) {
                        // Call notifyItemChanged() again in handler post delayed because item change animation doesn't
                        // occur when the view was not visible. Scroll to the view, then call notifyItemChanged which
                        // will animate the view again.
                        // Also, if the view was already animating, then it's already in focus, so no need to manually invoke it
                        adapter.notifyItemChanged(position)
                    }
                }, 200)
            }
            Timber.i("Student updated")
        })

        studentViewModel.eventShowDetailsFragment.observe(this, Observer {
            showDetailsFragment(it, true)
        })

        studentViewModel.eventStudentCardNotFound.observe(this, Observer {
            Timber.w("Card ($it) did not match any students")
            Toast.makeText(context, "Card did not match any students", Toast.LENGTH_SHORT).show()
        })

        studentViewModel.eventStudentAlreadyCheckedIn.observe(this, Observer {
            Timber.w("Student already checked in")
            Toast.makeText(context, "Student already checked in", Toast.LENGTH_SHORT).show()
        })
    }

    private fun showDetailsFragment(student: Student, isAwaitingGuardianCheckin: Boolean) {
        val detailsFragment = StudentDetailsFragment.getInstance()
        if(detailsFragment.isAdded) {
            // If another student swipes, dismiss current dialog before creating the new one
            detailsFragment.dismiss()
        }
        val bundle = Bundle()
        bundle.putLong("studentId", student.id)
        bundle.putLong("tripId", tripId)
        bundle.putEnum("boardingStatusMode", mode)
        bundle.putEnum("tripStatus", studentViewModel.tripStatus.value)
        bundle.putBoolean("isAwaitingGuardianCheckin", isAwaitingGuardianCheckin)
        detailsFragment.arguments = bundle
        Timber.d("Details bundle: $bundle")
        detailsFragment.show(fragmentManager!!, "Details")

        detailsFragment.onDismiss {
            // This is required because the recycler view will not be updated (visible changes)
            // when it's not visible on the screen. If the database is updated when the details fragment is
            // being shown, then the data update is received by the students view model, submitList is called,
            // but recycler view will appear unchanged.
            // So, manually notice when the dialog is closed and resubmit the list
            Timber.d("onDismiss")
            val handler = Handler()
            handler.postDelayed({
                val items = studentViewModel.students.value
                /* Updating with submit list
                if(items != null) {
                    adapter.setItems(items)
                }
                adapter.notifyDataSetChanged()
                */
                if(items != null) {
                    adapter.submitList(items)
                }
                Timber.i("Student list updated")
            }, 100)
        }

        detailsFragment.onStudentManuallyCheckin { checkedInStudent, guardian->
            studentViewModel.onStudentCheckin(checkedInStudent, guardian)
        }

        detailsFragment.onStudentCheckinCancel { checkedInStudent ->
            studentViewModel.onStudentCheckinCancelled(checkedInStudent)
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY, requestingLocationUpdates)
        super.onSaveInstanceState(outState)
    }

    private fun updateValuesFromBundle(savedInstanceState: Bundle?) {
        savedInstanceState ?: return

        // Update the value of requestingLocationUpdates from the Bundle.
        if (savedInstanceState.keySet().contains(REQUESTING_LOCATION_UPDATES_KEY)) {
            requestingLocationUpdates = savedInstanceState.getBoolean(
                REQUESTING_LOCATION_UPDATES_KEY)
        }
    }

    override fun onResume() {
        super.onResume()
        if (requestingLocationUpdates) startLocationUpdates()
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    private fun startLocationUpdates() {
        val activity = requireNotNull(activity)
        if (ContextCompat.checkSelfPermission(activity.applicationContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(activity.applicationContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Timber.w("Location permissions not enabled")
            return
        }
        Timber.i("Starting Location updates")
        fusedLocationClient.requestLocationUpdates(locationRequest,
            locationCallback,
            null /* Looper */)
    }

    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    private fun getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(requireNotNull(activity).applicationContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            requestingLocationUpdates = true
            showEnableLocationPrompt()
        } else {
            requestPermissions(
                Array(1){Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        requestingLocationUpdates = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    requestingLocationUpdates = true
                    showEnableLocationPrompt()
                    Timber.i("Location permission granted: $requestingLocationUpdates")
                    // startLocationUpdates()
                }
            }
        }
    }

    private fun showEnableLocationPrompt() {
        val settingsBuilder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        // settingsBuilder.setAlwaysShow(true);

        val result = LocationServices.getSettingsClient(requireNotNull(activity))
            .checkLocationSettings(settingsBuilder.build())

        result.addOnCompleteListener { task ->
            try {
                /*val result = */task.getResult(ApiException::class.java)
            } catch (ex: ApiException) {
                when (ex.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                        try {
                            val resolvableApiException = ex as ResolvableApiException
                            resolvableApiException.startResolutionForResult(activity, 1002)
                        } catch (e: IntentSender.SendIntentException) {
                            Timber.e(e)
                        }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {}
                }
            }
        }
    }
}
