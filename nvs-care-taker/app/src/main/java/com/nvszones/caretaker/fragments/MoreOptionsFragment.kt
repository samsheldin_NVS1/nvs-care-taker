package com.nvszones.caretaker.fragments


import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.Intent.getIntent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.preference.PreferenceManager
import com.nvszones.caretaker.LanguagePreference
import com.nvszones.caretaker.MainActivity
import com.nvszones.caretaker.R
import kotlinx.android.synthetic.main.fragment_more_options.*

/**
 * A simple [Fragment] subclass.
 *
 */
class MoreOptionsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_more_options, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonScanCard.setOnClickListener {
            findNavController().navigate(R.id.scanCardFragment)
        }

        buttonSynchronize.setOnClickListener {
            if(findNavController().currentDestination?.id != R.id.syncFragment) {
                // Snackbar.make(textRouteInput, "$it", Snackbar.LENGTH_SHORT).show()
                val prefs = PreferenceManager.getDefaultSharedPreferences(activity)
                val routeId = prefs.getLong("routeId", -1)
                findNavController().navigate(MoreOptionsFragmentDirections.actionMoreOptionsFragmentToSyncFragment(routeId))
            }
        }
        buttonChangeLanguage.setOnClickListener {
            showChangeLanguage(view.context)
        }
    }
    private fun showChangeLanguage(context: Context) {

        val listLanguage = arrayOf("English", "हिंदी", "ಕನ್ನಡ","தமிழ்","తెలుగు")

        val languagePopup = AlertDialog.Builder(context)
        languagePopup.setTitle(R.string.language_selection)
        languagePopup.setSingleChoiceItems(listLanguage, -1) { dialog, which ->
            if (which == 0) {
                setNewLocale(context, LanguagePreference.LANGUAGE_KEY_ENGLISH)
            } else if (which == 1) {
                setNewLocale(context, LanguagePreference.LANGUAGE_KEY_HINDI)
            } else if (which == 2) {
                setNewLocale(context, LanguagePreference.LANGUAGE_KEY_KANNADA)
            }else if (which == 3) {
                setNewLocale(context, LanguagePreference.LANGUAGE_KEY_TAMIL)
            }else if (which == 4) {
                setNewLocale(context, LanguagePreference.LANGUAGE_KEY_TELUGU)
            }

           // doRestart(context)
            // activity?.recreate()
            /*startActivity(
                Intent(activity, MainActivity::class.java)
                    .addFlags((Intent.FLAG_ACTIVITY_NEW_TASK )))*/

            dialog.dismiss()

        }
        val mDialog: AlertDialog = languagePopup.create()

        mDialog.show()

    }
    fun setNewLocale(mContext:Context,  language:String) {
        LanguagePreference.setNewLocale(mContext, language)
        val intent :Intent= Intent(activity,MainActivity::class.java)
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK))
    }


    fun doRestart(c: Context) {
        val mStartActivity = Intent(c, MainActivity::class.java)
        val mPendingIntentId = 123456
        val mPendingIntent = PendingIntent.getActivity(c, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT)
        val mgr = c.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 10, mPendingIntent)
        System.exit(0)
        /*run({
            try {
                if (c != null) {
                    val pm = c.getPackageManager()

                    if (pm != null) {

                        val mStartActivity = pm.getLaunchIntentForPackage(
                            c.getPackageName()
                        )
                        if (mStartActivity != null) {
                            mStartActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

                            val mPendingIntentId = 223344
                            val mPendingIntent = PendingIntent
                                .getActivity(
                                    c, mPendingIntentId, mStartActivity,
                                    PendingIntent.FLAG_CANCEL_CURRENT
                                )
                            val mgr = c.getSystemService(Context.ALARM_SERVICE) as AlarmManager
                            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent)

                            System.exit(0)
                        } else {
                            // Log.e(TAG, "Was not able to restart application, mStartActivity null")
                        }
                    } else {
                        // Log.e(TAG, "Was not able to restart application, PM null")
                    }
                } else {
                    //  Log.e(TAG, "Was not able to restart application, Context null")
                }
            } catch (ex: Exception) {
                // Log.e(TAG, "Was not able to restart application")
            }
        })*/

    }
}