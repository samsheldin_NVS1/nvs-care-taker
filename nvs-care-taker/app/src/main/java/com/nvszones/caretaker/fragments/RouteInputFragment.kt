package com.nvszones.caretaker.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.preference.PreferenceManager
import com.google.android.material.snackbar.Snackbar
import com.nvszones.caretaker.R
import com.nvszones.caretaker.viewmodels.RouteInputViewModel
import kotlinx.android.synthetic.main.fragment_route_input.*


class RouteInputFragment : Fragment() {

    private lateinit var routeInputViewModel: RouteInputViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        routeInputViewModel = ViewModelProviders.of(this).get(RouteInputViewModel::class.java)
        return inflater.inflate(R.layout.fragment_route_input, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button0.setOnClickListener { routeInputViewModel.addDigit(0) }
        button1.setOnClickListener { routeInputViewModel.addDigit(1) }
        button2.setOnClickListener { routeInputViewModel.addDigit(2) }
        button3.setOnClickListener { routeInputViewModel.addDigit(3) }
        button4.setOnClickListener { routeInputViewModel.addDigit(4) }
        button5.setOnClickListener { routeInputViewModel.addDigit(5) }
        button6.setOnClickListener { routeInputViewModel.addDigit(6) }
        button7.setOnClickListener { routeInputViewModel.addDigit(7) }
        button8.setOnClickListener { routeInputViewModel.addDigit(8) }
        button9.setOnClickListener { routeInputViewModel.addDigit(9) }

        buttonBackspace.setOnClickListener { routeInputViewModel.backspace() }
        buttonBackspace.setOnLongClickListener { routeInputViewModel.clearText(); false }
        buttonGo.setOnClickListener { routeInputViewModel.go() }

        routeInputViewModel.text.observe(this, Observer {
            textRouteInput.text = it
        })
        routeInputViewModel.eventMaxInputExceeded.observe(this, Observer {
            Snackbar.make(textRouteInput, "Route input number cannot be greater than $it digits", Snackbar.LENGTH_SHORT).show()
        })
        routeInputViewModel.eventNoInput.observe(this, Observer {
            Snackbar.make(textRouteInput, "Please enter a route number", Snackbar.LENGTH_SHORT).show()
        })
        routeInputViewModel.eventGo.observe(this, Observer {
            val navController = findNavController()
            if(navController.currentDestination?.id != R.id.syncFragment) {
                // Snackbar.make(textRouteInput, "$it", Snackbar.LENGTH_SHORT).show()
                val editor = PreferenceManager.getDefaultSharedPreferences(activity).edit()
                editor.putLong("routeId", it.toLong())
                editor.apply()
                navController.navigate(RouteInputFragmentDirections.actionRouteInputFragmentToSyncFragment(it.toLong()))
            }
        })
    }
}
