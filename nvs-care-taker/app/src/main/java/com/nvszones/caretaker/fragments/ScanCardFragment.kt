package com.nvszones.caretaker.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.nvszones.caretaker.R
import com.nvszones.caretaker.viewmodels.NfcEventViewModel
import com.nvszones.caretaker.viewmodels.ScanCardViewModel
import kotlinx.android.synthetic.main.fragment_scan_card.*

/**
 * A simple [Fragment] subclass.
 *
 */
class ScanCardFragment : Fragment() {

    private lateinit var scanCardViewModel: ScanCardViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        scanCardViewModel = ViewModelProviders.of(this).get(ScanCardViewModel::class.java)
        return inflater.inflate(R.layout.fragment_scan_card, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val nfcEventViewModel = ViewModelProviders.of(requireNotNull(activity)).get(NfcEventViewModel::class.java)
        nfcEventViewModel.eventNfcDiscovered.observe(this, Observer {
            textCardId.text = it
            scanCardViewModel.registerCard(it)
        })

        scanCardViewModel.eventCardRegister.observe(this, Observer {
            Toast.makeText(activity!!, it, Toast.LENGTH_SHORT).show()
        })
    }
}
