package com.nvszones.caretaker.fragments


import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.nvszones.caretaker.R
import com.nvszones.caretaker.adapters.GuardiansAdapter
import com.nvszones.caretaker.adapters.StudentsAdapter
import com.nvszones.caretaker.dao.CareTakerDatabase
import com.nvszones.caretaker.models.*
import com.nvszones.caretaker.network.BASE_URL
import com.nvszones.caretaker.utils.getEnum
import com.nvszones.caretaker.viewmodels.NfcEventViewModel
import com.nvszones.caretaker.viewmodels.StudentDetailsViewModel
import com.nvszones.caretaker.viewmodels.StudentViewModel
import com.nvszones.caretaker.viewmodels.StudentsDetailsViewModelFactory
import kotlinx.android.synthetic.main.fragment_student_details.*
import timber.log.Timber


/**
 * A simple [Fragment] subclass.
 *
 */
class StudentDetailsFragment : BottomSheetDialogFragment() {

    private lateinit var detailsViewModel: StudentDetailsViewModel
    private lateinit var nfcEventViewModel: NfcEventViewModel
    private lateinit var adapter: GuardiansAdapter
    private lateinit var boardingFragmentMode: StudentViewModel.BoardingFragmentMode
    private lateinit var tripStatus: Trip.TripStatus
    private var isAwaitingGuardianCheckinWhenOpen: Boolean = false

    private var onStudentManuallyCheckinListener: ((student: Student, guardian: Guardian?)->Unit)? = null

    private var onStudentCheckinCancelListener: ((student: Student)->Unit)? = null

    private var onDismissListener: (()->Unit)? = null

    fun onStudentManuallyCheckin(listener: ((student: Student, guardian: Guardian?)->Unit)?) {
        this.onStudentManuallyCheckinListener = listener
    }

    fun onStudentCheckinCancel(listener: ((student: Student)->Unit)?) {
        this.onStudentCheckinCancelListener = listener
    }

    fun onDismiss(listener: (()->Unit)?) {
        this.onDismissListener = listener
    }

    companion object {
        @Volatile
        private var INSTANCE: StudentDetailsFragment? = null

        fun getInstance(): StudentDetailsFragment {
            synchronized(this) {
                var instance = INSTANCE
                if(instance == null) {
                    instance = StudentDetailsFragment()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_student_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val studentId = arguments?.getLong("studentId", -1) ?: -1
        val tripId = arguments?.getLong("tripId", -1) ?: -1
        isAwaitingGuardianCheckinWhenOpen = arguments?.getBoolean("isAwaitingGuardianCheckin", false) ?: false
        // TODO: Illegal argument
        boardingFragmentMode = arguments?.getEnum("boardingStatusMode", StudentViewModel.BoardingFragmentMode.MODE_PICKUP_BOARDING) ?: StudentViewModel.BoardingFragmentMode.MODE_PICKUP_BOARDING
        tripStatus = arguments?.getEnum("tripStatus", Trip.TripStatus.TRIP_NOT_STARTED) ?: Trip.TripStatus.TRIP_NOT_STARTED
        val application = requireNotNull(activity).application
        val studentsDao = CareTakerDatabase.getInstance(application).studentsDao
        val guardiansDao = CareTakerDatabase.getInstance(application).guardiansDao

        val detailsViewModelFactory = StudentsDetailsViewModelFactory(studentId, tripId, boardingFragmentMode, studentsDao, guardiansDao, application)
        detailsViewModel = ViewModelProviders.of(this, detailsViewModelFactory).get(StudentDetailsViewModel::class.java)
        nfcEventViewModel = ViewModelProviders.of(requireNotNull(activity)).get(NfcEventViewModel::class.java)

        val showManualCheckinButton = tripStatus == Trip.TripStatus.TRIP_IN_PROGRESS && (boardingFragmentMode == StudentViewModel.BoardingFragmentMode.MODE_DROP_OFF || boardingFragmentMode == StudentViewModel.BoardingFragmentMode.MODE_GATE_DROP_OFF)
        adapter = GuardiansAdapter()
        adapter.showManualCheckinButton = showManualCheckinButton
        adapter.enableCheckin = false
        adapter.highlightCheckedIn = (boardingFragmentMode == StudentViewModel.BoardingFragmentMode.MODE_DROP_OFF || boardingFragmentMode == StudentViewModel.BoardingFragmentMode.MODE_GATE_DROP_OFF)
        val gridLayoutManager = GridLayoutManager(view.context, 3)
        recyclerGuardianList.adapter = adapter
        recyclerGuardianList.layoutManager = gridLayoutManager
        recyclerGuardianList.visibility = View.GONE
        textNoGuardiansToShow.visibility = View.VISIBLE

        buttonCancel.setOnClickListener {
            dismiss()
        }
        buttonClose.setOnClickListener {
            dismiss()
        }
        buttonManuallyBoardStudent.setOnClickListener {
            detailsViewModel.onStudentCheckin(true)
        }
        buttonCancelBoarding.setOnClickListener {
            detailsViewModel.onStudentCheckinCancel()
        }
        adapter.setOnClickListener {
            detailsViewModel.onGuardianCheckin(it, true)
        }


        textParentPhone.setOnClickListener {
            val phoneNumber: String? = detailsViewModel.primaryGuardian.value?.phoneNumber
            if(phoneNumber != null) {
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:$phoneNumber")
                startActivity(intent)
            }
        }

        if(tripStatus == Trip.TripStatus.TRIP_IN_PROGRESS) {
            buttonManuallyBoardStudent.visibility = View.VISIBLE
            layoutParentDetails.visibility = View.GONE
        } else {
            buttonManuallyBoardStudent.visibility = View.GONE
            layoutParentDetails.visibility = View.VISIBLE
        }

        setupObservers()
        if(isAwaitingGuardianCheckinWhenOpen) {
            val handler = Handler()
            handler.postDelayed({
                detailsViewModel.onStudentCheckin(false)
            }, 100) // This delay is necessary coz, otherwise the set values would be overwritten by other live data initialization
        }
    }

    private fun setupObservers() {
        detailsViewModel.guardians.observe(this, Observer {
            Timber.v(it.toString())
            /*if(adapter.itemCount == 0) {
                Timber.w("Submitting list")
                adapter.submitList(it)
            }*/
            adapter.submitList(null)
            adapter.submitList(it)
            val checkedInCount = it.count { guardian -> guardian.checkinStatus == CheckinStatus.CHECKED_IN }
            if(checkedInCount > 0) {
                adapter.enableCheckin = false
            }
            adapter.notifyDataSetChanged()

            if(it == null || it.isEmpty()) {
                recyclerGuardianList.visibility = View.GONE
                textNoGuardiansToShow.visibility = View.VISIBLE
            } else {
                recyclerGuardianList.visibility = View.VISIBLE
                textNoGuardiansToShow.visibility = View.GONE
            }
        })

        detailsViewModel.currentStudent.observe(this, Observer {
            if(it == null) {
                dismiss()
                return@Observer
            }
            if(it.selfDropEnabled) {
                adapter.showManualCheckinButton = false
                adapter.notifyDataSetChanged()
            }
            textStudentName.text = it.name
            textStudentClass.text = it.studentClass
            textStatus.text = when(it.boardingStatus) {
                BoardingStatus.NOT_BOARDED -> "Not Boarded"
                BoardingStatus.BOARDED -> "Boarded"
                BoardingStatus.SCHEDULED_TO_OFF_BOARD -> "Scheduled to drop off"
                BoardingStatus.OFF_BOARDED -> "Dropped off"
                BoardingStatus.ABSENT -> "Absent"
            }
            if(StudentsAdapter.isActive(it, boardingFragmentMode)) {
                /*buttonManuallyBoardStudent.text = when(boardingFragmentMode) {
                    StudentViewModel.BoardingFragmentMode.MODE_PICKUP_BOARDING -> "Already boarded"
                    StudentViewModel.BoardingFragmentMode.MODE_DROP_BOARDING -> "Already boarded"
                    StudentViewModel.BoardingFragmentMode.MODE_DROP_OFF -> "Already dropped off"
                }
                // buttonManuallyBoardStudent.setText(R.string.already_boarded)
                buttonManuallyBoardStudent.isEnabled = false*/
                if(tripStatus == Trip.TripStatus.TRIP_IN_PROGRESS) {
                    buttonManuallyBoardStudent.isEnabled = false
                    buttonCancelBoarding.isEnabled = true
                    buttonManuallyBoardStudent.visibility = View.GONE
                    buttonCancelBoarding.visibility = View.VISIBLE
                } else {
                    buttonManuallyBoardStudent.isEnabled = false
                    buttonCancelBoarding.isEnabled = false
                    buttonManuallyBoardStudent.visibility = View.GONE
                    buttonCancelBoarding.visibility = View.GONE
                }
                imageStudentPhoto.setBackgroundResource(R.drawable.bg_student_item_green)
                imageCheckInIndicator.setColorFilter(ContextCompat.getColor(imageCheckInIndicator.context, R.color.colorPrimaryDark))
            } else {
                // buttonManuallyBoardStudent.setText(R.string.board_manually)
                buttonManuallyBoardStudent.text = when(boardingFragmentMode) {
                    StudentViewModel.BoardingFragmentMode.MODE_PICKUP_BOARDING -> "Board manually"
                    StudentViewModel.BoardingFragmentMode.MODE_DROP_BOARDING -> "Board manually"
                    StudentViewModel.BoardingFragmentMode.MODE_DROP_OFF -> "Drop off manually"
                    StudentViewModel.BoardingFragmentMode.MODE_GATE_BOARDING -> "Enter manually"
                    StudentViewModel.BoardingFragmentMode.MODE_GATE_DROP_OFF -> "Exit manually"
                }
                buttonManuallyBoardStudent.isEnabled = true

                if(tripStatus == Trip.TripStatus.TRIP_IN_PROGRESS) {
                    buttonManuallyBoardStudent.isEnabled = true
                    buttonCancelBoarding.isEnabled = false
                    buttonManuallyBoardStudent.visibility = View.VISIBLE
                    buttonCancelBoarding.visibility = View.GONE
                } else {
                    buttonManuallyBoardStudent.isEnabled = false
                    buttonCancelBoarding.isEnabled = false
                    buttonManuallyBoardStudent.visibility = View.GONE
                    buttonCancelBoarding.visibility = View.GONE
                }

                imageStudentPhoto.setBackgroundResource(R.drawable.bg_student_item_grey)
                imageCheckInIndicator.clearColorFilter()
            }


            if(it.isManualCheckin && StudentsAdapter.isActive(it, boardingFragmentMode)) {
                imageManualCheckinIndicator.setColorFilter(ContextCompat.getColor(imageCheckInIndicator.context, R.color.colorPrimaryDark))
            } else {
                imageManualCheckinIndicator.clearColorFilter()
            }
            if(!it.selfDropEnabled) {
                imageStudentSecureIndicator.setColorFilter(ContextCompat.getColor(imageCheckInIndicator.context, R.color.colorPrimaryDark))
            } else {
                imageStudentSecureIndicator.clearColorFilter()
            }
            if(it.isCrossBoarded) {
                imageCrossBoardIndicator.setColorFilter(ContextCompat.getColor(imageCheckInIndicator.context, R.color.colorPrimaryDark))
                imageCrossBoardIndicator.visibility = View.VISIBLE
            } else {
                imageCrossBoardIndicator.clearColorFilter()
                imageCrossBoardIndicator.visibility = View.GONE
            }

            if(it.photoUrl.isNullOrEmpty()) {
                if (it.gender == Gender.MALE) {
                    imageStudentPhoto.setActualImageResource(R.drawable.default_icon_boy)
                } else {
                    imageStudentPhoto.setActualImageResource(R.drawable.default_icon_girl)
                }
            } else {
                val photoUrl = BASE_URL + it.photoUrl
                imageStudentPhoto.setImageURI(photoUrl)
            }
            if(it.gender == Gender.MALE) {
                imageStudentPhoto.hierarchy.setFailureImage(R.drawable.default_icon_boy)
            } else {
                imageStudentPhoto.hierarchy.setFailureImage(R.drawable.default_icon_girl)
            }

        })

        detailsViewModel.primaryGuardian.observe(this, Observer {
            if(it != null) {
                textParentName.text = it.name
                textParentPhone.text = it.phoneNumber
            }
        })

        detailsViewModel.eventOnCheckedIn.observe(this, Observer {
            onStudentManuallyCheckinListener?.invoke(it.student, it.guardian)
            if(isAwaitingGuardianCheckinWhenOpen) {
                // Close when the fragment is already waiting for guardian checkin i.e, opened when student swiped and
                // the dialog was auto opened
                dismiss()
            }
        })

        detailsViewModel.eventOnCheckedInCancelled.observe(this, Observer {
            onStudentCheckinCancelListener?.invoke(it.student)
        })

        detailsViewModel.eventAwaitingGuardianCheckin.observe(this, Observer { isAwaiting ->
            if(isAwaiting) {
                buttonManuallyBoardStudent.setText(R.string.awaiting_guardian_checkin)
                buttonManuallyBoardStudent.isEnabled = false
                adapter.enableCheckin = true
                adapter.notifyDataSetChanged()

                val bottomSheetDialog: BottomSheetDialog? = dialog as BottomSheetDialog
                val bottomSheet = bottomSheetDialog?.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)
                if(bottomSheet != null) {
                    BottomSheetBehavior.from(bottomSheet).state = BottomSheetBehavior.STATE_EXPANDED
                }
            } else {
                Timber.e(IllegalStateException("isAwaiting is false"))
            }
        })

        nfcEventViewModel.eventNfcDiscovered.observe(this, Observer {
            Timber.i("Handling NFC in details fragment")
            if(buttonManuallyBoardStudent.isEnabled && buttonManuallyBoardStudent.visibility == View.VISIBLE) {
                if (detailsViewModel.currentStudent.value?.cardId.equals(it, true)) {
                    detailsViewModel.onStudentCheckin(false)
                } else {
                    Timber.i("Card $it did not match the current student")
                    Toast.makeText(context, "Card did not match the current student", Toast.LENGTH_SHORT).show()
                }
            } else if(adapter.enableCheckin && adapter.showManualCheckinButton) {
                detailsViewModel.onGuardianCardDiscovered(it)
            } else {
                Timber.i("Ignoring card in details fragment")
            }
        })

        detailsViewModel.eventGuardianCardNotFound.observe(this, Observer {
            Timber.w("Card ($it) did not match any guardians for the student")
            Toast.makeText(context, "Card did not match any guardians", Toast.LENGTH_SHORT).show()
        })

        detailsViewModel.eventGuardianAlreadyCheckedIn.observe(this, Observer {
            Timber.w("Guardian already checked in")
            Toast.makeText(context, "Guardian already checked in", Toast.LENGTH_SHORT).show()
        })
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onDismissListener?.invoke()
    }
}
