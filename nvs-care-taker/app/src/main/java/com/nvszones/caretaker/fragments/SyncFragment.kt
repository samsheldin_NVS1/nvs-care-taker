package com.nvszones.caretaker.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.snackbar.Snackbar
import com.nvszones.caretaker.R
import com.nvszones.caretaker.dao.CareTakerDatabase
import com.nvszones.caretaker.viewmodels.MainActivityViewModel
import com.nvszones.caretaker.viewmodels.MainActivityViewModelFactory
import com.nvszones.caretaker.viewmodels.SyncViewModel
import com.nvszones.caretaker.viewmodels.SyncViewModelFactory
import kotlinx.android.synthetic.main.fragment_sync.*
import timber.log.Timber

/**
 * A simple [Fragment] subclass.
 *
 */
class SyncFragment : Fragment() {

    private lateinit var syncViewModel: SyncViewModel

    private val args: SyncFragmentArgs by navArgs()
    private var routeId: Long = -1
    private var awaitingTransactionsPostComplete = false

    // TODO: Initiate sync and report issue
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sync, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        routeId = args.routeId
        if(routeId == -1L) {
            Timber.w("Route Id is -1")
        } else {
            Timber.i("Sync on routeId: $routeId")
        }
        val application = requireNotNull(activity).application
        val tripsDao = CareTakerDatabase.getInstance(application).tripsDao
        val studentsDao = CareTakerDatabase.getInstance(application).studentsDao
        val guardiansDao = CareTakerDatabase.getInstance(application).guardiansDao
        val transactionsDao = CareTakerDatabase.getInstance(application).transactionsDao
        val activity = requireNotNull(activity)

        val syncViewModelFactory = SyncViewModelFactory(routeId, tripsDao, studentsDao, guardiansDao, transactionsDao, application)
        val mainViewModelFactory = MainActivityViewModelFactory(transactionsDao, application)

        syncViewModel = ViewModelProviders.of(this, syncViewModelFactory).get(SyncViewModel::class.java)
        val mainViewModel = ViewModelProviders.of(activity, mainViewModelFactory).get(MainActivityViewModel::class.java)
        setupObservers()

        buttonStartSync.setOnClickListener {
            // syncViewModel.clearTransactions()
            if(mainViewModel.totalDirtyTransactions.value?:1 > 0) {
                mainViewModel.postCheckinTransactions(true)
                Timber.i("Awaiting checkin transactions to be pushed")
                awaitingTransactionsPostComplete = true
                handleSyncUiUpdate(
                    SyncViewModel.SyncStatus.SYNC_IN_PROGRESS,
                    progressTransactionSync,
                    imageTransactionSyncStatus
                )
            } else {
                syncEverthingElse()
                awaitingTransactionsPostComplete = false
                handleSyncUiUpdate(SyncViewModel.SyncStatus.SYNC_SUCCESS, progressTransactionSync, imageTransactionSyncStatus)
            }
        }

        mainViewModel.totalDirtyTransactions.observe(this, Observer {
            if(it <= 0 && awaitingTransactionsPostComplete) {
                syncViewModel.clearTransactions()
                syncEverthingElse()
                awaitingTransactionsPostComplete = false
                handleSyncUiUpdate(SyncViewModel.SyncStatus.SYNC_SUCCESS, progressTransactionSync, imageTransactionSyncStatus)
            }
        })
        mainViewModel.eventManualTransactionsPushFailed.observe(this, Observer {
            handleSyncUiUpdate(SyncViewModel.SyncStatus.SYNC_FAILURE, progressTransactionSync, imageTransactionSyncStatus)
        })

        buttonClose.setOnClickListener {
            imageCloud.findNavController().navigate(R.id.action_syncFragment_to_tripsFragment)
        }

        textLastUpdated.text = getString(R.string.last_updated, syncViewModel.lastSyncTimeText)
    }

    private fun syncEverthingElse() {
        syncViewModel.fetchTrips()
        syncViewModel.fetchStudents()
        syncViewModel.fetchGuardians()
    }

    private fun setupObservers() {
        syncViewModel.trips.observe(this, Observer {
            Timber.i(it.toString())
        })

        syncViewModel.students.observe(this, Observer {
            Timber.i(it.toString())
        })

        syncViewModel.guardians.observe(this, Observer {
            Timber.i("Guardians: $it")
        })

        syncViewModel.tripsSyncStatus.observe(this, Observer {
            handleSyncUiUpdate(it, progressTripsSync, imageTripsSyncStatus)
        })

        syncViewModel.studentsSyncStatus.observe(this, Observer {
            handleSyncUiUpdate(it, progressStudentSync, imageStudentSyncStatus)
        })

        syncViewModel.guardiansSyncStatus.observe(this, Observer {
            handleSyncUiUpdate(it, progressGuardianSync, imageGuardianSyncStatus)
        })

        syncViewModel.eventNetworkFetchError.observe(this, Observer {
            Snackbar.make(imageCloud, it, Snackbar.LENGTH_SHORT).show()
        })

        syncViewModel.overallSyncStatus.observe(this, Observer {
            when(it) {
                SyncViewModel.SyncStatus.SYNC_NOT_STARTED -> {
                    buttonStartSync.visibility = View.VISIBLE
                    buttonStartSync.isEnabled = true
                    buttonStartSync.setText(R.string.begin_sync)
                    if(syncViewModel.isSyncNecessary) {
                        buttonClose.visibility = View.GONE
                    } else {
                        buttonClose.visibility = View.VISIBLE
                    }
                }
                SyncViewModel.SyncStatus.SYNC_SUCCESS -> {
                    buttonStartSync.visibility = View.GONE
                    buttonStartSync.isEnabled = false
                    buttonClose.visibility = View.VISIBLE
                    textLastUpdated.visibility = View.INVISIBLE
                    syncViewModel.saveSyncTime()
                }
                SyncViewModel.SyncStatus.SYNC_FAILURE -> {
                    buttonStartSync.visibility = View.VISIBLE
                    buttonStartSync.setText(R.string.retry_sync)
                    buttonStartSync.isEnabled = true
                    if(syncViewModel.isSyncNecessary) {
                        buttonClose.visibility = View.GONE
                    } else {
                        buttonClose.visibility = View.VISIBLE
                    }
                }
                SyncViewModel.SyncStatus.SYNC_IN_PROGRESS -> {
                    buttonStartSync.visibility = View.VISIBLE
                    buttonStartSync.isEnabled = false
                    buttonClose.visibility = View.INVISIBLE
                }
                else -> {
                    buttonStartSync.visibility = View.VISIBLE
                    buttonStartSync.isEnabled = true
                    buttonStartSync.setText(R.string.begin_sync)
                    if(syncViewModel.isSyncNecessary) {
                        buttonClose.visibility = View.GONE
                    } else {
                        buttonClose.visibility = View.VISIBLE
                    }
                }
            }
        })
    }

    private fun handleSyncUiUpdate(syncStatus: SyncViewModel.SyncStatus, progress: ProgressBar, imageView: AppCompatImageView) {
        when(syncStatus) {
            SyncViewModel.SyncStatus.SYNC_IN_PROGRESS, SyncViewModel.SyncStatus.SYNC_STARTED -> {
                progress.visibility = View.VISIBLE
                imageView.visibility = View.GONE
            }

            SyncViewModel.SyncStatus.SYNC_SUCCESS -> {
                progress.visibility = View.GONE
                imageView.visibility = View.VISIBLE
                imageView.setImageResource(R.drawable.ic_check_circle_accent_24dp)
            }

            SyncViewModel.SyncStatus.SYNC_FAILURE -> {
                progress.visibility = View.GONE
                imageView.visibility = View.VISIBLE
                imageView.setImageResource(R.drawable.ic_cancel_red_24dp)
            }

            else -> {
                progress.visibility = View.GONE
                imageView.visibility = View.VISIBLE
                imageView.setImageResource(R.drawable.ic_access_time_grey_24dp)
            }
        }
    }
}
