package com.nvszones.caretaker.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.nvszones.caretaker.R
import com.nvszones.caretaker.adapters.TripsAdapter
import com.nvszones.caretaker.dao.CareTakerDatabase
import com.nvszones.caretaker.models.Trip
import com.nvszones.caretaker.viewmodels.StudentViewModel
import com.nvszones.caretaker.viewmodels.TripsViewModel
import com.nvszones.caretaker.viewmodels.TripsViewModelFactory
import kotlinx.android.synthetic.main.fragment_trips.*
import timber.log.Timber

/**
 * A simple [Fragment] subclass.
 *
 */
class TripsFragment : Fragment() {

    private lateinit var tripsAdapter: TripsAdapter
    private lateinit var tripsViewModel: TripsViewModel

    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val application = requireNotNull(this.activity).application

        val tripsDao = CareTakerDatabase.getInstance(application).tripsDao

        val tripsViewModelFactory = TripsViewModelFactory(tripsDao, application)

        tripsViewModel = ViewModelProviders.of(this, tripsViewModelFactory).get(TripsViewModel::class.java)

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_trips, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tripsAdapter = TripsAdapter()
        recyclerTrips.adapter = tripsAdapter

        tripsViewModel.trips.observe(this, Observer {
            Timber.i(it.toString())
            tripsAdapter.submitList(it)

            if(it == null || it.isEmpty()) {
                recyclerTrips.visibility = View.GONE
                textNoTripsToShow.visibility = View.VISIBLE
            } else {
                recyclerTrips.visibility = View.VISIBLE
                textNoTripsToShow.visibility = View.GONE
            }
        })

        navController = Navigation.findNavController(view)

        tripsAdapter.setOnBoardingClickListener {
            val action = TripsFragmentDirections.actionTripsFragmentToPickupBoardingFragment()
            action.tripId = it.id
            if(it.tripType == Trip.TripType.TRIP_TYPE_GATE_ENTRY) {
                action.mode = StudentViewModel.BoardingFragmentMode.MODE_GATE_BOARDING
            } else {
                action.mode = StudentViewModel.BoardingFragmentMode.MODE_PICKUP_BOARDING
            }
            navController.navigate(action)
        }

        tripsAdapter.setOnDropBoardingClickListener {
            val action = TripsFragmentDirections.actionTripsFragmentToPickupBoardingFragment()
            action.tripId = it.id
            action.mode = StudentViewModel.BoardingFragmentMode.MODE_DROP_BOARDING
            navController.navigate(action)
        }

        tripsAdapter.setOnDropOffClickListener {
            val action = TripsFragmentDirections.actionTripsFragmentToPickupBoardingFragment()
            action.tripId = it.id
            if(it.tripType == Trip.TripType.TRIP_TYPE_GATE_EXIT) {
                action.mode = StudentViewModel.BoardingFragmentMode.MODE_GATE_DROP_OFF
            } else {
                action.mode = StudentViewModel.BoardingFragmentMode.MODE_DROP_OFF
            }
            navController.navigate(action)
        }

        buttonMoreOptions.setOnClickListener {
            navController.navigate(R.id.moreOptionsFragment)
        }

//        val trips = mutableListOf<Trip>()
//        for (i in 0..10) {
//            trips.add(Trip(i.toLong(), "Route $i", "Pickup $i", listOf(Trip.TRIP_TYPE_PICKUP, Trip.TRIP_TYPE_DROP).shuffled()[0], "2019-10-22T01:37:56", "2019-10-22T01:57:56"))
//        }
//        tripsAdapter.submitList(trips)
    }
}
