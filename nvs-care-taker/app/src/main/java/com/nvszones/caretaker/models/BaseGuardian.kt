package com.nvszones.caretaker.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity(tableName = "guardians")
data class BaseGuardian(
    @PrimaryKey
    @field:Json(name="guardian_id")
    var id: Long,

    @ColumnInfo
    @field:Json(name="name")
    var name: String,

    @ColumnInfo
    @field:Json(name="gender")
    var gender: Gender,

    @ColumnInfo
    @field:Json(name="card_id")
    var cardId: String,

    @ColumnInfo
    @field:Json(name="photo_url")
    var photoUrl: String?,

    @ColumnInfo
    @field:Json(name="phone")
    var phoneNumber: String
)

enum class CheckinStatus { NOT_CHECKED_IN, CHECKED_IN }