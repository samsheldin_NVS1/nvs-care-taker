package com.nvszones.caretaker.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity(tableName = "students")
data class BaseStudent(
    @PrimaryKey
    @field:Json(name="student_id")
    var id: Long,

    @ColumnInfo
    @field:Json(name="name")
    var name: String,

    @ColumnInfo
    @field:Json(name="student_class")
    var studentClass: String,

    @ColumnInfo
    @field:Json(name="gender")
    var gender: Gender,

    @ColumnInfo
    @field:Json(name="card_id")
    var cardId: String,

    @ColumnInfo
    @field:Json(name="photo_url")
    var photoUrl: String?,

    @ColumnInfo
    @field:Json(name="self_drop_enabled")
    var selfDropEnabled: Boolean
)