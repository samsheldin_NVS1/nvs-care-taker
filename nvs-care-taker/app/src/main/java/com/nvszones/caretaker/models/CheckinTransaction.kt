package com.nvszones.caretaker.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.nvszones.caretaker.viewmodels.StudentViewModel
import com.squareup.moshi.Json

@Entity(tableName = "checkin_transactions")
data class CheckinTransaction(

    @ColumnInfo
    @field:Json(name="entityId")
    var entityId: Long, // Student id or guardian id

    @ColumnInfo
    @field:Json(name="entityType")
    var entityType: EntityType,

    @ColumnInfo
    @field:Json(name="reference_id")
    var referenceId: Long, // In case of guardian entity, this will have the student id. Otherwise, unused

    @ColumnInfo
    @field:Json(name="tripId")
    var tripId: Long,

    @ColumnInfo
    @field:Json(name="transactionType")
    var transactionType: TransactionType,

    @ColumnInfo
    @field:Json(name="boardingFragmentMode")
    var boardingFragmentMode: StudentViewModel.BoardingFragmentMode,

    @ColumnInfo
    @field:Json(name="entityCard")
    var cardId: String, // The reduce 1 extra query on the server side

    @ColumnInfo
    @field:Json(name="isManualCheckin")
    var isManualCheckin: Boolean,

    @ColumnInfo
    @field:Json(name="latitude")
    var latitude: Double = -1.0,

    @ColumnInfo
    @field:Json(name="longitude")
    var longitude: Double = -1.0,

    @ColumnInfo
    @field:Json(name="locationUpdatedTime")
    var locationUpdatedTime: Long = -1L,

    @ColumnInfo
    @field:Json(name="dirty")
    var dirty: Boolean = true,

    @PrimaryKey(autoGenerate = true)
    @field:Json(name="AppTransactionID")
    var id: Long = 0,

    @ColumnInfo
    @field:Json(name="timestamp")
    var timestamp: Long = System.currentTimeMillis(),

    @field:Json(name="Appid")
    var imei: String = "12"
)

enum class EntityType { STUDENT, GUARDIAN, UNKNOWN }
enum class TransactionType { CHECKIN, CANCEL_CHECKIN, UNKNOWN }