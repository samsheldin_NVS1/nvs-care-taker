package com.nvszones.caretaker.models

data class Guardian(
    var id: Long,
    var name: String,
    var gender: Gender,
    var relationship: String,
    var cardId: String,
    var photoUrl: String?,
    var isPrimaryGuardian: Boolean,
    var phoneNumber: String,
    var checkinStatus: CheckinStatus,
    var isManualCheckin: Boolean
    // var _relId: Long = -1 // Id to identify row from relationship table, no other use. Mainly for debugging
)