package com.nvszones.caretaker.models

import com.squareup.moshi.Json

data class NewScannedCard(
    @field:Json(name="nfccard_no")
    var cardId: String
)