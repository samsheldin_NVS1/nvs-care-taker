package com.nvszones.caretaker.models

import com.squareup.moshi.Json

data class ServerResponse (
    @field:Json(name="Sucess")
    var success: Boolean,

    @field:Json(name="Message")
    var message: String,

    @field:Json(name="TranscationRefrence")
    var transactionId: Long
)