package com.nvszones.caretaker.models

import androidx.room.TypeConverter
import com.nvszones.caretaker.viewmodels.StudentViewModel
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

data class Student(
    var id: Long,
    var name: String,
    var studentClass: String,
    var gender: Gender,
    var cardId: String,
    var photoUrl: String?,
    var selfDropEnabled: Boolean,
    var boardingStatus: BoardingStatus,
    var isManualCheckin: Boolean,
    var isCrossBoarded: Boolean
    // var _relId: Long = -1 // Id to identify row from relationship table, no other use. Mainly for debugging
)

enum class Gender { MALE, FEMALE }

enum class BoardingStatus { NOT_BOARDED, BOARDED, SCHEDULED_TO_OFF_BOARD, OFF_BOARDED, ABSENT }

class Converter {
    companion object {
        @TypeConverter
        @JvmStatic
        @FromJson
        fun toGender(gender: String?): Gender {
            return when(gender) {
                "male" -> Gender.MALE
                "female" -> Gender.FEMALE
                else -> Gender.MALE
            }
        }

        @TypeConverter
        @JvmStatic
        @ToJson
        fun fromGender(gender: Gender?): String {
            return when(gender) {
                Gender.MALE -> "male"
                Gender.FEMALE -> "female"
                else -> "male"
            }
        }

        @TypeConverter
        @JvmStatic
        @FromJson
        fun toBoardingStatus(boardingStatus: Int?): BoardingStatus {
            return when(boardingStatus) {
                0 -> BoardingStatus.NOT_BOARDED
                1 -> BoardingStatus.BOARDED
                2 -> BoardingStatus.SCHEDULED_TO_OFF_BOARD
                3 -> BoardingStatus.OFF_BOARDED
                4 -> BoardingStatus.ABSENT
                else -> BoardingStatus.NOT_BOARDED
            }
        }

        @TypeConverter
        @JvmStatic
        @ToJson
        fun fromBoardingStatus(boardingStatus: BoardingStatus?): Int {
            return when(boardingStatus) {
                BoardingStatus.NOT_BOARDED -> 0
                BoardingStatus.BOARDED -> 1
                BoardingStatus.SCHEDULED_TO_OFF_BOARD -> 2
                BoardingStatus.OFF_BOARDED -> 3
                BoardingStatus.ABSENT -> 4
                else -> 0
            }
        }

        @TypeConverter
        @JvmStatic
        @FromJson
        fun toTripType(tripType: Int?): Trip.TripType {
            return when(tripType) {
                1 -> Trip.TripType.TRIP_TYPE_PICKUP
                2 -> Trip.TripType.TRIP_TYPE_DROP
                3 -> Trip.TripType.TRIP_TYPE_GATE_ENTRY
                4 -> Trip.TripType.TRIP_TYPE_GATE_EXIT
                else -> Trip.TripType.TRIP_TYPE_UNKNOWN
            }
        }

        @TypeConverter
        @JvmStatic
        @ToJson
        fun fromTripType(tripType: Trip.TripType): Int {
            return when(tripType) {
                Trip.TripType.TRIP_TYPE_PICKUP -> 1
                Trip.TripType.TRIP_TYPE_DROP -> 2
                Trip.TripType.TRIP_TYPE_GATE_ENTRY -> 3
                Trip.TripType.TRIP_TYPE_GATE_EXIT -> 4
                else -> -1
            }
        }

        @TypeConverter
        @JvmStatic
        @FromJson
        fun toTripStatus(tripStatus: Int?): Trip.TripStatus {
            return when(tripStatus) {
                /*"trip_not_started" -> Trip.TripStatus.TRIP_NOT_STARTED
                "trip_in_progress" -> Trip.TripStatus.TRIP_IN_PROGRESS
                "trip_ended" -> Trip.TripStatus.TRIP_ENDED
                else -> Trip.TripStatus.TRIP_NOT_STARTED*/
                // Changing string to integer on server team requirement
                0 -> Trip.TripStatus.TRIP_NOT_STARTED
                1 -> Trip.TripStatus.TRIP_IN_PROGRESS
                2 -> Trip.TripStatus.TRIP_ENDED
                else -> Trip.TripStatus.TRIP_NOT_STARTED
            }
        }

        @TypeConverter
        @JvmStatic
        @ToJson
        fun fromTripStatus(tripStatus: Trip.TripStatus?): Int {
            return when(tripStatus) {
                /*Trip.TripStatus.TRIP_NOT_STARTED -> "trip_not_started"
                Trip.TripStatus.TRIP_IN_PROGRESS -> "trip_in_progress"
                Trip.TripStatus.TRIP_ENDED -> "trip_ended"
                else -> "trip_not_started"*/

                Trip.TripStatus.TRIP_NOT_STARTED -> 0
                Trip.TripStatus.TRIP_IN_PROGRESS -> 1
                Trip.TripStatus.TRIP_ENDED -> 2
                else -> 0
            }
        }

        @TypeConverter
        @JvmStatic
        @FromJson
        fun toCheckinStatus(checkinStatus: Int?): CheckinStatus {
            return when(checkinStatus) {
                0 -> CheckinStatus.NOT_CHECKED_IN
                1 -> CheckinStatus.CHECKED_IN
                else -> CheckinStatus.NOT_CHECKED_IN
            }
        }

        @TypeConverter
        @JvmStatic
        @ToJson
        fun fromCheckinStatus(checkinStatus: CheckinStatus ?): Int {
            return when(checkinStatus) {
                CheckinStatus.NOT_CHECKED_IN -> 0
                CheckinStatus.CHECKED_IN -> 1
                else -> 0
            }
        }

        @TypeConverter
        @JvmStatic
        @FromJson
        fun toEntityType(entityType: String?): EntityType {
            return when(entityType) {
                "student" -> EntityType.STUDENT
                "guardian" -> EntityType.GUARDIAN
                else -> EntityType.UNKNOWN
            }
        }

        @TypeConverter
        @JvmStatic
        @ToJson
        fun fromEntityType(entityType: EntityType ?): String {
            return when(entityType) {
                EntityType.STUDENT -> "student"
                EntityType.GUARDIAN -> "guardian"
                else -> "unknown"
            }
        }

        @TypeConverter
        @JvmStatic
        @FromJson
        fun toBoardingFragmentMode(mode: String?): StudentViewModel.BoardingFragmentMode {
            return when(mode) {
                "pickup_boarding" -> StudentViewModel.BoardingFragmentMode.MODE_PICKUP_BOARDING
                "drop_boarding" -> StudentViewModel.BoardingFragmentMode.MODE_DROP_BOARDING
                "drop_off" -> StudentViewModel.BoardingFragmentMode.MODE_DROP_OFF
                "gate_entry" -> StudentViewModel.BoardingFragmentMode.MODE_GATE_BOARDING
                "gate_exit" -> StudentViewModel.BoardingFragmentMode.MODE_GATE_DROP_OFF
                else -> StudentViewModel.BoardingFragmentMode.MODE_PICKUP_BOARDING
            }
        }

        @TypeConverter
        @JvmStatic
        @ToJson
        fun fromBoardingFragmentMode(boardingFragmentMode: StudentViewModel.BoardingFragmentMode ?): String {
            return when(boardingFragmentMode) {
                StudentViewModel.BoardingFragmentMode.MODE_PICKUP_BOARDING -> "pickup_boarding"
                StudentViewModel.BoardingFragmentMode.MODE_DROP_BOARDING -> "drop_boarding"
                StudentViewModel.BoardingFragmentMode.MODE_DROP_OFF -> "drop_off"
                StudentViewModel.BoardingFragmentMode.MODE_GATE_BOARDING -> "gate_entry"
                StudentViewModel.BoardingFragmentMode.MODE_GATE_DROP_OFF -> "gate_exit"
                else -> "pickup_boarding"
            }
        }

        @TypeConverter
        @JvmStatic
        @FromJson
        fun toTransactionType(mode: Int?): TransactionType {
            return when(mode) {
                // "checkin" -> TransactionType.CHECKIN
                // "cancel_checkin" -> TransactionType.CANCEL_CHECKIN
                // Changing string to integer for server side
                1 -> TransactionType.CHECKIN
                -1 -> TransactionType.CANCEL_CHECKIN
                else -> TransactionType.UNKNOWN
            }
        }

        @TypeConverter
        @JvmStatic
        @ToJson
        fun fromTransactionType(transactionType: TransactionType ?): Int {
            return when(transactionType) {
                // TransactionType.CHECKIN -> "checkin"
                // TransactionType.CANCEL_CHECKIN -> "cancel_checkin"
                // else -> "unknown"
                TransactionType.CHECKIN -> 1
                TransactionType.CANCEL_CHECKIN -> -1
                else -> -2
            }
        }
    }
}