package com.nvszones.caretaker.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity(tableName = "student_guardian_relationship")
data class StudentGuardianRelationship(
    @ColumnInfo
    @field:Json(name="student_id")
    var studentId: Long,

    @ColumnInfo
    @field:Json(name="guardian_id")
    var guardianId: Long,

    @ColumnInfo
    @field:Json(name="relationship")
    var relationship: String,

    @ColumnInfo
    @field:Json(name="primary_guardian")
    var isPrimaryGuardian: Boolean,

    @ColumnInfo
    @field:Json(name="checkin_status")
    var checkinStatus: CheckinStatus,

    @ColumnInfo
    @field:Json(name="ismanual_checkin")
    var isManualCheckin: Boolean,

    @PrimaryKey(autoGenerate = true)
    var _relId: Long = 0
)