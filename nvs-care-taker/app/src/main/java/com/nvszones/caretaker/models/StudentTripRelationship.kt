package com.nvszones.caretaker.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity(tableName = "student_trip_relationship")
data class StudentTripRelationship(
    @ColumnInfo
    @field:Json(name="studentid")
    var studentId: Long,

    @ColumnInfo
    @field:Json(name="tripid")
    var tripId: Long,

    @ColumnInfo
    @field:Json(name="boardingStatus")
    var boardingStatus: BoardingStatus,

    @ColumnInfo
    @field:Json(name="ismanualcheckin")
    var isManualCheckin: Boolean,

    @ColumnInfo
    @field:Json(name="isCrossBorded")
    var isCrossBoarded: Boolean,

    @PrimaryKey(autoGenerate = true)
    var _relId: Long = 0
)