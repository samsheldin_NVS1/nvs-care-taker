package com.nvszones.caretaker.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.nvszones.caretaker.BuildConfig
import com.squareup.moshi.Json
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

@Entity(tableName = "trips")
data class Trip(
    @PrimaryKey
    @field:Json(name="trip_id")
    var id: Long,

    @ColumnInfo
    @field:Json(name="route_id")
    var routeId: Long,

    @ColumnInfo
    @field:Json(name="trip_name")
    var tripName: String,

    @ColumnInfo
    @field:Json(name="trip_friendly_text")
    var tripFriendlyText: String,

    @ColumnInfo
    @field:Json(name="trip_type")
    var tripType: TripType,

    @ColumnInfo
    @field:Json(name="start_time")
    val startTime: String,

    @ColumnInfo
    @field:Json(name="end_time")
    val endTime: String,

    @ColumnInfo
    @field:Json(name="total_scheduled")
    var totalScheduled: Int = 0
) {
    @Ignore
    @Transient
    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US)

    @Ignore
    @Transient
    val friendlyTimeFormat = SimpleDateFormat("hh:mm a", Locale.US)

    fun getStartTimeLong(): Long {
        return try {
            sdf.parse(startTime)?.time ?: -1
        } catch (e: Exception) {
            Timber.e("Error converting start time to millis: $e")
            -1
        }
    }

    fun getEndTimeLong(): Long {
        return try {
            sdf.parse(endTime)?.time ?: -1
        } catch (e: Exception) {
            Timber.e("Error converting end time to millis: $e")
            -1
        }
    }

    fun isStartTripAllowed(): Boolean {
        val currentMillis = System.currentTimeMillis()
        return ((getStartTimeLong() <= currentMillis && currentMillis <= getEndTimeLong())) || BuildConfig.DEBUG
    }

    fun getFriendlyStartTime(): String {
        var friendlyTime = this.startTime
        try {
            friendlyTime = friendlyTimeFormat.format(sdf.parse(startTime)!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return friendlyTime
    }

    fun getFriendlyEndTime(): String {
        var friendlyTime = this.endTime
        try {
            friendlyTime = friendlyTimeFormat.format(requireNotNull(sdf.parse(endTime)))
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return friendlyTime
    }

    /*companion object {
        val TRIP_TYPE_PICKUP = 1
        val TRIP_TYPE_DROP = 2
        val TRIP_TYPE_GATE_ENTRY = 3
        val TRIP_TYPE_GATE_EXIT = 4
    }*/

    enum class TripType { TRIP_TYPE_PICKUP, TRIP_TYPE_DROP, TRIP_TYPE_GATE_ENTRY, TRIP_TYPE_GATE_EXIT, TRIP_TYPE_UNKNOWN }

    enum class TripStatus { TRIP_NOT_STARTED, TRIP_IN_PROGRESS, TRIP_ENDED }
}