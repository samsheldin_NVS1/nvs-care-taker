package com.nvszones.caretaker.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.nvszones.caretaker.viewmodels.StudentViewModel
import com.squareup.moshi.Json

@Entity(tableName = "trip_event_transactions")
data class TripEventTransaction(

    @ColumnInfo
    @field:Json(name="TripID")
    var tripId: Long, // trip id

    @ColumnInfo
    @field:Json(name="TripStatus")
    var tripStatus: Trip.TripStatus,

    @ColumnInfo
    @field:Json(name="boarding_fragment_mode")
    var boardingFragmentMode: StudentViewModel.BoardingFragmentMode,

    @ColumnInfo
    @field:Json(name="latitude")
    var latitude: Double = -1.0,

    @ColumnInfo
    @field:Json(name="longitude")
    var longitude: Double = -1.0,

    @ColumnInfo
    @field:Json(name="locationUpdatedTime")
    var locationUpdatedTime: Long = -1L,

    @ColumnInfo
    @field:Json(name="dirty")
    var dirty: Boolean = true,

    @PrimaryKey(autoGenerate = true)
    @field:Json(name="AppTransactionID")
    var id: Long = 0,

    @ColumnInfo
    @field:Json(name="timestamp")
    var timestamp: Long = System.currentTimeMillis(),

    @field:Json(name="Appid")
    var imei: String = "12"
)