package com.nvszones.caretaker.network

import com.nvszones.caretaker.models.*
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*


interface NVSCareTakerService {
    @GET("/api/GetRoutesListRouteName2/{routeId}")
    suspend fun getTrips(@Path("routeId") routeId: Long): Response<List<Trip>>

    @GET("/api/GetStudentForAllRoute")
    suspend fun getStudents(@Query("routeId") routeId: Long): Response<List<BaseStudent>>

    @GET("/api/GetAllGuardian")
    suspend fun getGuardians(@Query("routeId") routeId: Long): Response<List<BaseGuardian>>

    @GET("/api/GetStudentTripRelationship")
    suspend fun getStudentTripRelationships(@Query("routeId") routeId: Long): Response<List<StudentTripRelationship>>

    @GET("/api/StudentGuardianRelationship")
    suspend fun getStudentGuardianRelationships(@Query("routeId") routeId: Long): Response<List<StudentGuardianRelationship>>

    @POST("/api/CheckinTransaction")
    fun postCheckinTransactions(@Body checkinTransactions: List<CheckinTransaction>, @Query("id") imei: String): Call<List<ServerResponse>>

    @POST("/api/Change_State")
    fun postTripEventTransactions(@Body tripEventTransaction: List<TripEventTransaction>, @Query("id") imei: String): Call<List<ServerResponse>>

    @POST("/api/ADDNFCCard")
    suspend fun postRegisterNFC(@Body card: NewScannedCard): Response<ServerResponse>
}