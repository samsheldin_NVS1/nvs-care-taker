package com.nvszones.caretaker.utils

import android.util.Log.*
import com.crashlytics.android.Crashlytics
import timber.log.Timber


class CrashReportingTree : Timber.Tree() {

    override fun log(priority: Int, tag: String?, message: String, throwable: Throwable?) {
        when (priority) {
            VERBOSE, DEBUG, INFO -> { }
            WARN -> logWarning(priority, tag, message)
            ERROR -> logException(throwable)
            else -> {
            }
        }
    }

    private fun logWarning(priority: Int, tag: String?, message: String) {
        Crashlytics.log(priority, tag, message)
    }

    private fun logException(throwable: Throwable?) {
        Crashlytics.logException(throwable)
    }
}