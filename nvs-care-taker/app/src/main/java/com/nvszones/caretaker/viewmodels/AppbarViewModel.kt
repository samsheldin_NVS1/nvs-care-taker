package com.nvszones.caretaker.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.nvszones.caretaker.BuildConfig
import com.nvszones.caretaker.R

class AppbarViewModel(application: Application): AndroidViewModel(application) {
    val appbarTitle = MutableLiveData<String>()
    val appbarSubtitle = MutableLiveData<String>()
    val appVersion = MutableLiveData<String>()

    init {
        appbarTitle.value = application.resources.getString(R.string.school_name)
        appbarSubtitle.value = application.resources.getString(R.string.school_address)
        if(BuildConfig.DEBUG) {
            appVersion.value = BuildConfig.VERSION_NAME + "_"
            appbarSubtitle.value = appbarSubtitle.value + " - " + BuildConfig.serverUrl
        } else {
            appVersion.value = BuildConfig.VERSION_NAME
        }
    }
}
