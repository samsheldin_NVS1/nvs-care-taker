package com.nvszones.caretaker.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.preference.PreferenceManager
import com.nvszones.caretaker.dao.CareTakerDatabase
import com.nvszones.caretaker.dao.TransactionsDao
import com.nvszones.caretaker.network.NetworkUtils
import com.nvszones.caretaker.network.enqueue
import com.nvszones.caretaker.utils.LiveEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

const val MAX_TRANSACTIONS_PER_REQUEST = 50
const val PREF_KEY_IMEI_NUMBER = "deviceId"

class MainActivityViewModel(transactionsDao: TransactionsDao, val app: Application): AndroidViewModel(app) {
    val dirtyCheckinTransactions = transactionsDao.getAllDirtyCheckinTransactions()
    val dirtyTripEventTransactions = transactionsDao.getAllDirtyTripEventTransactions()

    val totalDirtyTransactions = MediatorLiveData<Int>()

    private val _onManualTransactionsPushFailed = LiveEvent<String>()
    val eventManualTransactionsPushFailed: LiveData<String> = _onManualTransactionsPushFailed

    init {
        totalDirtyTransactions.addSource(dirtyCheckinTransactions) {
            totalDirtyTransactions.value = (dirtyCheckinTransactions.value?.size ?: 0) + (dirtyTripEventTransactions.value?.size ?: 0)
        }

        totalDirtyTransactions.addSource(dirtyTripEventTransactions) {
            totalDirtyTransactions.value = (dirtyCheckinTransactions.value?.size ?: 0) + (dirtyTripEventTransactions.value?.size ?: 0)
        }
    }

    fun postCheckinTransactions(isManual: Boolean) {
        // First transactions data
        val transactionsSublist = dirtyCheckinTransactions.value?.take(MAX_TRANSACTIONS_PER_REQUEST)
        if(transactionsSublist != null && transactionsSublist.isNotEmpty()) {
            Timber.i("Posting: ${transactionsSublist.map { it.id }}")
            val imei = PreferenceManager.getDefaultSharedPreferences(app).getString(PREF_KEY_IMEI_NUMBER, "")
            if(imei.isNullOrEmpty()) {
                Timber.w("Can't post checkin transactions because IMEI is empty")
            } else {
                Timber.i("Posting checkin transactions (IMEI: $imei)")
                // TODO: Add descriptive error messages
                NetworkUtils.apiService.postCheckinTransactions(transactionsSublist, imei).enqueue {
                    onResponse = { response ->
                        if(response.isSuccessful) {
                            val results = response.body()
                            Timber.i("Response: $results")
                            results?.forEach {
                                if (it.success) {
                                    Timber.i("Posting checkin transactions successful: $it")
                                    GlobalScope.launch {
                                        withContext(Dispatchers.IO) {
                                            // TODO: Update conditionally, not map
                                            val successIds = transactionsSublist.filter { transaction -> transaction.id == it.transactionId}.map { transaction -> transaction.id }
                                            Timber.i("Updating checkin transactions: $successIds")
                                            CareTakerDatabase.getInstance(getApplication())
                                                .transactionsDao.updateDirtyCheckinTransactions(successIds)
                                        }
                                    }
                                } else {
                                    val errorMessage = "Error inserting checkin transaction ${it.transactionId}: ${it.message}"
                                    /*if (isManual) {
                                        _onManualTransactionsPushFailed.value = errorMessage
                                    }*/
                                    Timber.w(errorMessage)
                                }
                            }
                        } else {
                            Timber.w("Posting checkin transactions failed: $response")
                            if (isManual) {
                                _onManualTransactionsPushFailed.value =
                                    "Unable to push checkin data to the server (E009${response.code()})"
                            }
                        }
                    }

                    onFailure = {
                        it?.printStackTrace()
                        Timber.w("Posting checkin transactions failed (error): $it")
                        if(isManual) {
                            _onManualTransactionsPushFailed.value = "Unable to push checkin data to the server (E0001). Please check network connectivity"
                        }
                    }
                }
            }
        }
        else {
            Timber.d("No checkin transactions to push")
        }

        // Trip event transactions data
        val tripEventsSublist = dirtyTripEventTransactions.value?.take(MAX_TRANSACTIONS_PER_REQUEST)
        if(tripEventsSublist != null && tripEventsSublist.isNotEmpty()) {
            Timber.i("Posting: ${tripEventsSublist.map { it.id }}")
            val imei = PreferenceManager.getDefaultSharedPreferences(app).getString(PREF_KEY_IMEI_NUMBER, "")
            if(imei.isNullOrEmpty()) {
                Timber.w("Can't post trip event transactions because IMEI is empty")
            } else {
                NetworkUtils.apiService.postTripEventTransactions(tripEventsSublist, imei).enqueue {
                    onResponse = { response ->
                        if(response.isSuccessful) {
                            val results = response.body()
                            results?.forEach {
                                if (it.success) {
                                    Timber.i("Posting trip events transactions successful ($imei): $it")
                                    GlobalScope.launch {
                                        withContext(Dispatchers.IO) {
                                            val successIds = tripEventsSublist.filter { transaction -> transaction.id == it.transactionId}.map { transaction -> transaction.id }
                                            Timber.i("Updating trip event transactions: $successIds")
                                            CareTakerDatabase.getInstance(getApplication())
                                                .transactionsDao.updateDirtyTripEventTransactions(successIds)
                                        }
                                    }
                                } else {
                                    val errorMessage = "Error inserting trip event ${it.transactionId}: ${it.message}"
                                    /*if (isManual) {
                                        _onManualTransactionsPushFailed.value = errorMessage
                                    }*/
                                    Timber.w(errorMessage)
                                }
                            }
                        } else {
                            Timber.w("Posting trip event transactions failed: $response")
                            if (isManual) {
                                _onManualTransactionsPushFailed.value =
                                    "Unable to push trip events data to the server (E009${response.code()})"
                            }
                        }
                    }

                    onFailure = {
                        it?.printStackTrace()
                        Timber.w("Posting trip event transactions failed (error): $it")
                        if (isManual) {
                            _onManualTransactionsPushFailed.value =
                                "Unable to push trip events data to the server (E0001). Please check network connectivity"
                        }
                    }
                }
            }
        }
        else {
            Timber.d("No trip event transactions to push")
        }
    }
}
