package com.nvszones.caretaker.viewmodels

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nvszones.caretaker.dao.TransactionsDao

class MainActivityViewModelFactory (
    private val transactionsDao: TransactionsDao,
    private val application: Application ): ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(MainActivityViewModel::class.java)) {
            return MainActivityViewModel(transactionsDao, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}