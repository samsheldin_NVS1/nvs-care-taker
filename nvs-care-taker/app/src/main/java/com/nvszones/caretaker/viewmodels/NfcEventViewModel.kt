package com.nvszones.caretaker.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.nvszones.caretaker.utils.LiveEvent

class NfcEventViewModel(): ViewModel() {
    private val _nfcCard = LiveEvent<String>()
    val eventNfcDiscovered: LiveData<String> = _nfcCard

    fun nfcDiscovered(serialNumber: String) {
        _nfcCard.value = serialNumber
    }
}
