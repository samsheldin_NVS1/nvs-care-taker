package com.nvszones.caretaker.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nvszones.caretaker.utils.LiveEvent
import timber.log.Timber

class RouteInputViewModel: ViewModel() {
    private val MAX_ROUTE_INPUT_SIZE = 5

    var text = MutableLiveData<String>()

    private val _maxInputLimit = LiveEvent<Int>()
    val eventMaxInputExceeded: LiveData<Int> = _maxInputLimit

    private val _noInput = LiveEvent<Unit>()
    val eventNoInput: LiveData<Unit> = _noInput

    private val _eventGo = LiveEvent<Int>()
    val eventGo: LiveData<Int> = _eventGo

    init {
        text.value = ""
    }

    fun addDigit(digit: Int) {
        Timber.d("Route key pressed: $digit")
        if((text.value?.length ?: 0) < MAX_ROUTE_INPUT_SIZE) {
            text.value = text.value + digit
        } else {
            Timber.i("Max route key input exceeded")
            _maxInputLimit.value = MAX_ROUTE_INPUT_SIZE
        }
    }

    fun go() {
        try {
            if((text.value?.length ?: 0) > 0) {
                val routeNumber = Integer.parseInt(text.value as String)
                _eventGo.value = routeNumber
            } else {
                _noInput.value = Unit
                Timber.i("No route input")
            }
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    fun clearText() {
        text.value = ""
    }

    fun backspace() {
        if((text.value?.length ?: 0) > 0) {
            text.value = text.value?.substring(0, (text.value?.length ?: 1) - 1)
        } else {
            text.value = ""
        }
    }
}