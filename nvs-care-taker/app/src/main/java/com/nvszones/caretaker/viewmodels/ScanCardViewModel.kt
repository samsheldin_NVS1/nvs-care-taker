package com.nvszones.caretaker.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.nvszones.caretaker.models.NewScannedCard
import com.nvszones.caretaker.network.NetworkUtils
import com.nvszones.caretaker.utils.LiveEvent
import kotlinx.coroutines.*
import timber.log.Timber

class ScanCardViewModel: ViewModel() {

    private val _cardPostingMessage = LiveEvent<String>()
    val eventCardRegister: LiveData<String> = _cardPostingMessage

    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    fun registerCard(cardId: String) {
        uiScope.launch {
            withContext(Dispatchers.Main) {
                Timber.i("Posting scanned card: $cardId")
                try {
                    val response = NetworkUtils.apiService.postRegisterNFC(NewScannedCard(cardId))
                    if(response.isSuccessful) {
                        val results = response.body()
                        Timber.i("Scanned card posting results: $results")
                        _cardPostingMessage.value = results?.message ?: ""
                    } else {
                        Timber.w("Posting scan card failed: $response")
                        _cardPostingMessage.value = "Error sending the card to server. (E2${response.code()})"
                    }
                } catch (e: Exception) {
                    _cardPostingMessage.value = "Can't connect to server"
                }
            }
        }
    }
}
