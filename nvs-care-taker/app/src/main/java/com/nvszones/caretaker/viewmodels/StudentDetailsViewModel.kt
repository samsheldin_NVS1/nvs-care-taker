package com.nvszones.caretaker.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.nvszones.caretaker.dao.GuardiansDao
import com.nvszones.caretaker.dao.StudentsDao
import com.nvszones.caretaker.models.CheckinStatus
import com.nvszones.caretaker.models.Guardian
import com.nvszones.caretaker.models.Student
import com.nvszones.caretaker.utils.LiveEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import timber.log.Timber

class StudentDetailsViewModel(studentId: Long, tripId: Long, private val boardingFragmentMode: StudentViewModel.BoardingFragmentMode, studentsDao: StudentsDao, guardiansDao: GuardiansDao, application: Application): AndroidViewModel(application) {
    var currentStudent = studentsDao.get(studentId, tripId)
    var guardians = guardiansDao.getAllGuardians(studentId)
    var primaryGuardian = guardiansDao.getPrimaryGuardian(studentId)

    private val detailsViewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + detailsViewModelJob)

    private val _onCheckedIn = LiveEvent<CheckinTransaction>()
    val eventOnCheckedIn: LiveData<CheckinTransaction> = _onCheckedIn

    private val _onCancelCheckedIn = LiveEvent<CheckinTransaction>()
    val eventOnCheckedInCancelled: LiveData<CheckinTransaction> = _onCancelCheckedIn

    private val _guardianCardNotFound = LiveEvent<String>()
    val eventGuardianCardNotFound: LiveData<String> = _guardianCardNotFound

    private val _guardianAlreadyCheckedIn = LiveEvent<Guardian>()
    val eventGuardianAlreadyCheckedIn: LiveEvent<Guardian> = _guardianAlreadyCheckedIn

    private val _awaitingGuardianCheckin = LiveEvent<Boolean>()
    val eventAwaitingGuardianCheckin: LiveData<Boolean> = _awaitingGuardianCheckin

    init {
    }

    fun onStudentCheckin(isManual: Boolean) {
        if(boardingFragmentMode == StudentViewModel.BoardingFragmentMode.MODE_PICKUP_BOARDING ||
            boardingFragmentMode == StudentViewModel.BoardingFragmentMode.MODE_DROP_BOARDING ||
            (boardingFragmentMode == StudentViewModel.BoardingFragmentMode.MODE_DROP_OFF && currentStudent.value?.selfDropEnabled == true) ||
            (boardingFragmentMode == StudentViewModel.BoardingFragmentMode.MODE_GATE_DROP_OFF && currentStudent.value?.selfDropEnabled == true) ||
            boardingFragmentMode == StudentViewModel.BoardingFragmentMode.MODE_GATE_BOARDING) {
            val student: Student? = currentStudent.value
            if (student != null) {
                student.isManualCheckin = isManual
                _onCheckedIn.value = CheckinTransaction(student)
            } else {
                Timber.w("Invoked onStudentCheckin with null")
            }
        } else {
            Timber.i("Awaiting student checkin")
            _awaitingGuardianCheckin.value = true
        }
    }

    fun onGuardianCardDiscovered(cardId: String) {
        Timber.i("Guardian card discovered: $cardId")
        if(guardians.value != null) {
            var isCardFound = false
            for(guardian in guardians.value!!) {
                if(guardian.cardId.equals(cardId, true)) {
                    if(guardian.checkinStatus != CheckinStatus.CHECKED_IN) {
                        onGuardianCheckin(guardian, false)
                        Timber.i("Found guardian: $guardian")
                    } else {
                        _guardianAlreadyCheckedIn.value = guardian
                    }
                    isCardFound = true
                    break
                }
            }
            if(!isCardFound) {
                _guardianCardNotFound.value = cardId
            }
        }
    }

    fun onGuardianCheckin(guardian: Guardian, isManual: Boolean) {
        if(_awaitingGuardianCheckin.value == true) {
            guardian.isManualCheckin = isManual
            val student: Student? = currentStudent.value
            if (student != null) {
                _onCheckedIn.value = CheckinTransaction(student, guardian)
            } else {
                Timber.w("Invoked onStudentCheckin with null")
            }
        } else {
            Timber.e(IllegalStateException("guardian checkin while student is not checked in"))
        }
    }

    fun onStudentCheckinCancel() {
        val student: Student? = currentStudent.value
        if (student != null) {
            student.isManualCheckin = true
            Timber.i("Cancelling checking for student: $student")
            _onCancelCheckedIn.value = CheckinTransaction(student)
        } else {
            Timber.w("Invoked onStudentCheckinCancel with null")
        }
    }

    override fun onCleared() {
        super.onCleared()
        detailsViewModelJob.cancel()
    }

    data class CheckinTransaction(val student: Student, val guardian: Guardian? = null)
}