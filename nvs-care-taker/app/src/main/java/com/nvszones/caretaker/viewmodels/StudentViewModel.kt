package com.nvszones.caretaker.viewmodels

import android.app.Application
import android.location.Location
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.nvszones.caretaker.adapters.StudentsAdapter
import com.nvszones.caretaker.dao.CareTakerDatabase
import com.nvszones.caretaker.models.*
import com.nvszones.caretaker.utils.LiveEvent
import kotlinx.coroutines.*
import timber.log.Timber

class StudentViewModel(private val mode: BoardingFragmentMode, private val tripId: Long, private val careTakerDatabase: CareTakerDatabase, application: Application): AndroidViewModel(application) {
    var students: LiveData<List<Student>>
    private val tripsDao = careTakerDatabase.tripsDao
    private val studentsDao = careTakerDatabase.studentsDao
    private val guardiansDao = careTakerDatabase.guardiansDao
    private val transactionsDao = careTakerDatabase.transactionsDao

    val sortState = MutableLiveData<StudentListSortState>()

    var lastKnownLocation: Location? = null

    var currentTrip = tripsDao.get(tripId)

    private val studentViewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + studentViewModelJob)

    val _studentUpdated = LiveEvent<Student>()
    val eventStudentUpdated: LiveData<Student> = _studentUpdated

    private val _showDetailsFragment = LiveEvent<Student>()
    val eventShowDetailsFragment: LiveData<Student> = _showDetailsFragment

    private val _studentCardNotFound = LiveEvent<String>()
    val eventStudentCardNotFound: LiveEvent<String> = _studentCardNotFound

    private val _studentAlreadyCheckedIn = LiveEvent<Student>()
    val eventStudentAlreadyCheckedIn: LiveEvent<Student> = _studentAlreadyCheckedIn

    // TODO: Don't show start trip and end trip outside trip timings
    var tripStatus = MutableLiveData<Trip.TripStatus>()

    private val _errorUpdatingStudent = LiveEvent<String>()
    val eventErrorUpdatingStudent: LiveData<String> = _errorUpdatingStudent

    private val _tripNotStarted = LiveEvent<Boolean>()
    val eventTripNotStarted: LiveData<Boolean> = _tripNotStarted

    private val _errorStartStopTrip = LiveEvent<String>()
    val eventErrorStartingStoppingTrip: LiveData<String> = _errorStartStopTrip

    val countScheduled: Int
        get() {
            return if(mode == BoardingFragmentMode.MODE_GATE_DROP_OFF || mode == BoardingFragmentMode.MODE_DROP_OFF) {
                students.value?.size ?: 0
            } else {
                students.value?.count { student -> !student.isCrossBoarded } ?: 0
            }
        }

    val countBoarded: Int
        get() {
            return if(mode == BoardingFragmentMode.MODE_GATE_DROP_OFF || mode == BoardingFragmentMode.MODE_DROP_OFF) {
                students.value?.count { student -> StudentsAdapter.isActive(student, mode) } ?: 0
            } else {
                students.value?.count { student -> StudentsAdapter.isActive(student, mode) && !student.isCrossBoarded } ?: 0
            }
        }

    val countMissed: Int
        get() {
            return if(mode == BoardingFragmentMode.MODE_GATE_DROP_OFF || mode == BoardingFragmentMode.MODE_DROP_OFF) {
                students.value?.count { student -> !StudentsAdapter.isActive(student, mode) } ?: 0
            } else {
                students.value?.count { student -> !StudentsAdapter.isActive(student, mode) && !student.isCrossBoarded } ?: 0
            }
        }

    val countCrossBoarded: Int
        get() {
            return if(mode == BoardingFragmentMode.MODE_GATE_DROP_OFF || mode == BoardingFragmentMode.MODE_DROP_OFF) {
                0 // No crossboarding in drop off trips
            } else {
                return students.value?.count { student -> student.isCrossBoarded } ?: 0
            }
        }

    init {
        tripStatus.value = Trip.TripStatus.TRIP_NOT_STARTED
        students = when(mode) {
            BoardingFragmentMode.MODE_PICKUP_BOARDING -> studentsDao.getAllStudents(tripId)
            BoardingFragmentMode.MODE_DROP_BOARDING -> studentsDao.getAllStudents(tripId)
            BoardingFragmentMode.MODE_DROP_OFF -> studentsDao.getDropOffStudents(tripId)
            BoardingFragmentMode.MODE_GATE_BOARDING -> studentsDao.getAllStudents(tripId)
            BoardingFragmentMode.MODE_GATE_DROP_OFF -> studentsDao.getAllStudents(tripId)
        }
        sortState.value = StudentListSortState.ALPHABETICAL
        // students = studentsDao.getAllStudents(tripId)
        Timber.i("Student Fragment Mode: $mode")
    }

    fun onTripStartStopClick() {
        uiScope.launch {
            tripStatus.value = when(tripStatus.value) {
                Trip.TripStatus.TRIP_NOT_STARTED -> {
                    try {
                        if(currentTrip.value?.isStartTripAllowed() == true) {
                            Trip.TripStatus.TRIP_IN_PROGRESS
                        } else {
                            Timber.e("Cannot start trip. Outside the trip timings")
                            _errorStartStopTrip.value = "Not allowed to start the trip outside the trip timings"
                            Trip.TripStatus.TRIP_NOT_STARTED
                        }
                    } catch (e: Exception) {
                        Timber.e("Error starting the trip")
                        _errorStartStopTrip.value = "Error starting trip"
                        Trip.TripStatus.TRIP_NOT_STARTED
                    }
                }
                Trip.TripStatus.TRIP_IN_PROGRESS -> Trip.TripStatus.TRIP_ENDED
                // TODO: Introduce unknown trip
                Trip.TripStatus.TRIP_ENDED, null -> Trip.TripStatus.TRIP_NOT_STARTED
            }
            val count = withContext(Dispatchers.IO) {
                val latitude: Double = lastKnownLocation?.latitude ?: -1.0
                val longitude: Double = lastKnownLocation?.longitude ?: -1.0
                val time: Long = lastKnownLocation?.time ?: -1L

                val newState = tripStatus.value!! // Can't be null coz trip status was updated above
                val transaction = TripEventTransaction(tripId, newState, mode, latitude, longitude, time)
                Timber.i("Inserting new trip event transaction: $transaction")
                transactionsDao.insert(transaction)
            }
            if(count <= 0) {
                _errorStartStopTrip.value = "Data error while starting the trip"
                Timber.e(RuntimeException("Error starting the trip. Database state can't be recorded"))
            }
        }
    }

    private fun updateDropoffCheckin(student: Student, guardian: Guardian) {
        uiScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    // Timber.i("Transaction: $student, $guardian")
                    careTakerDatabase.runInTransaction {
                        var count = guardiansDao.updateCheckinStatus(student.id, guardian.id, guardian.checkinStatus, guardian.isManualCheckin)
                        if(count <= 0) {
                            throw RuntimeException("Error updating guardian checkin status while drop off")
                        }
                        count = studentsDao.updateBoardingStatus(student.id, tripId, student.boardingStatus, student.isManualCheckin, student.isCrossBoarded)
                        if(count <= 0) {
                            throw RuntimeException("Error updating student boarding status while drop off")
                        }
                        val latitude: Double = lastKnownLocation?.latitude ?: -1.0
                        val longitude: Double = lastKnownLocation?.longitude ?: -1.0
                        val time: Long = lastKnownLocation?.time ?: -1L

                        val studentTransaction = CheckinTransaction(student.id, EntityType.STUDENT, -1, tripId, TransactionType.CHECKIN, mode, student.cardId, student.isManualCheckin, latitude, longitude, time)
                        val guardianTransaction = CheckinTransaction(guardian.id, EntityType.GUARDIAN, student.id, tripId, TransactionType.CHECKIN, mode, guardian.cardId, guardian.isManualCheckin, latitude, longitude, time)
                        transactionsDao.insert(studentTransaction)
                        transactionsDao.insert(guardianTransaction)

                        Timber.i("Current transaction: $studentTransaction")
                        Timber.i("Current transaction: $guardianTransaction")
                    }
                }
                Timber.i("Student update successful for drop off: $student, $guardian")
                _studentUpdated.value = student
            } catch (e: Exception) {
                _errorUpdatingStudent.value = "Error updating student information"
                Timber.e("Error updating student for drop off: $student, $guardian")
                Timber.e(e)
            }
        }
    }

    private fun updateStudent(student: Student) {
        uiScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    // Timber.i("Transaction: $student")
                    careTakerDatabase.runInTransaction {
                        val latitude: Double = lastKnownLocation?.latitude ?: -1.0
                        val longitude: Double = lastKnownLocation?.longitude ?: -1.0
                        val time: Long = lastKnownLocation?.time ?: -1L
                        val transaction = CheckinTransaction(student.id, EntityType.STUDENT, -1, tripId, TransactionType.CHECKIN, mode, student.cardId, student.isManualCheckin, latitude, longitude, time)
                        transactionsDao.insert(transaction)
                        Timber.i("Current transaction: $transaction")

                        if(student.isCrossBoarded) {
                            val temp = studentsDao.getStudent(student.id, tripId)
                            // Timber.w("Temp read: $temp")
                            if(temp == null) { // Relationship doesn't exist yet, so create it
                                val rel = StudentTripRelationship(
                                    student.id,
                                    tripId,
                                    student.boardingStatus,
                                    student.isManualCheckin,
                                    true
                                )
                                studentsDao.insert(rel)
                                Timber.i("Inserting student-trip relationship because, the student is cross boarding: $rel")
                            } else {
                                Timber.i("Crossboarded student relationship already exist $temp")
                            }
                        }

                        val count = studentsDao.updateBoardingStatus(student.id, tripId, student.boardingStatus, student.isManualCheckin, student.isCrossBoarded)
                        Timber.d("Affected rows: $count")
                        if(count <= 0) {
                            throw RuntimeException("Update failed")
                        }
                    }
                }
                _studentUpdated.value = student
                Timber.i("Student update successful: $student")
            } catch (e: Exception) {
                _errorUpdatingStudent.value = "Error updating student information"
                Timber.e("Error updating student: $student")
                Timber.e(e)
            }
        }
    }

    fun onStudentCardDiscovered(cardId: String) {
        if(tripStatus.value == Trip.TripStatus.TRIP_IN_PROGRESS) {
            uiScope.launch {
                if(students.value != null) {
                    var isCardFound = false
                    var foundStudent: Student? = null
                    for(student in students.value!!) {
                        if(student.cardId.equals(cardId, true)) {
                            foundStudent = student
                            isCardFound = true
                            break
                        }
                    }
                    if(!isCardFound && (mode == BoardingFragmentMode.MODE_PICKUP_BOARDING || mode == BoardingFragmentMode.MODE_DROP_BOARDING || mode == BoardingFragmentMode.MODE_GATE_BOARDING)) {
                        Timber.i("Searching for possible cross boarding")
                        val baseStudent = withContext(Dispatchers.IO) {
                            studentsDao.get(cardId)
                        }
                        if(baseStudent != null) {
                            val defaultBoardingStatus = when(mode) {
                                BoardingFragmentMode.MODE_PICKUP_BOARDING, BoardingFragmentMode.MODE_GATE_BOARDING -> BoardingStatus.NOT_BOARDED
                                BoardingFragmentMode.MODE_DROP_BOARDING, BoardingFragmentMode.MODE_GATE_DROP_OFF -> BoardingStatus.BOARDED
                                BoardingFragmentMode.MODE_DROP_OFF -> BoardingStatus.SCHEDULED_TO_OFF_BOARD
                            }
                            val student = Student(baseStudent.id, baseStudent.name, baseStudent.studentClass, baseStudent.gender, baseStudent.cardId, baseStudent.photoUrl, baseStudent.selfDropEnabled, defaultBoardingStatus, isManualCheckin = false, isCrossBoarded = true)
                            foundStudent = student
                            isCardFound = true
                            Timber.i("Found cross boarding entry: $student")
                        }
                    }
                    if(isCardFound && foundStudent != null) {
                        if(!StudentsAdapter.isActive(foundStudent, mode)) {
                            foundStudent.isManualCheckin = false
                            if ((mode == BoardingFragmentMode.MODE_DROP_OFF || mode == BoardingFragmentMode.MODE_GATE_DROP_OFF) && !foundStudent.selfDropEnabled) {
                                _showDetailsFragment.value = foundStudent
                            } else {
                                onStudentCheckin(foundStudent, null)
                            }
                        } else {
                            _studentAlreadyCheckedIn.value = foundStudent
                        }
                    } else {
                        _studentCardNotFound.value = cardId
                    }
                }
            }
        } else {
            _tripNotStarted.value = true
            Timber.i("Trip not started")
        }
    }

    fun onStudentCheckin(student: Student, guardian: Guardian?) {
        if(tripStatus.value == Trip.TripStatus.TRIP_IN_PROGRESS) {
            // student.boardingStatus = if (student.boardingStatus == BoardingStatus.BOARDED) BoardingStatus.NOT_BOARDED else BoardingStatus.BOARDED
            student.boardingStatus = when(mode) {
                BoardingFragmentMode.MODE_PICKUP_BOARDING -> {
                    if (student.boardingStatus == BoardingStatus.NOT_BOARDED) BoardingStatus.BOARDED else student.boardingStatus
                }

                BoardingFragmentMode.MODE_DROP_BOARDING -> {
                    // if (student.boardingStatus == BoardingStatus.BOARDED) BoardingStatus.SCHEDULED_TO_OFF_BOARD else student.boardingStatus
                    BoardingStatus.SCHEDULED_TO_OFF_BOARD
                }

                BoardingFragmentMode.MODE_DROP_OFF -> {
                    guardian?.checkinStatus = CheckinStatus.CHECKED_IN
                    if (student.boardingStatus == BoardingStatus.SCHEDULED_TO_OFF_BOARD) BoardingStatus.OFF_BOARDED else student.boardingStatus
                }

                BoardingFragmentMode.MODE_GATE_BOARDING -> {
                    if (student.boardingStatus == BoardingStatus.NOT_BOARDED) BoardingStatus.BOARDED else student.boardingStatus
                }

                BoardingFragmentMode.MODE_GATE_DROP_OFF -> {
                    guardian?.checkinStatus = CheckinStatus.CHECKED_IN
                    BoardingStatus.OFF_BOARDED
                }
            }

            when(mode) {
                BoardingFragmentMode.MODE_DROP_BOARDING, BoardingFragmentMode.MODE_PICKUP_BOARDING, BoardingFragmentMode.MODE_GATE_BOARDING -> updateStudent(student)
                BoardingFragmentMode.MODE_DROP_OFF, BoardingFragmentMode.MODE_GATE_DROP_OFF-> {
                    if(student.selfDropEnabled) {
                        updateStudent(student)
                    } else {
                        updateDropoffCheckin(student, guardian!!)
                    }
                }
            }
        } else {
            _tripNotStarted.value = true
            Timber.i("Trip has not started")
        }
    }

    private fun cancelStudentUpdate(student: Student, cancelGuardians: Boolean, removeStudentTripRelationship: Boolean) {
        uiScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    careTakerDatabase.runInTransaction {
                        val count = studentsDao.updateBoardingStatus(student.id, tripId, student.boardingStatus, student.isManualCheckin, student.isCrossBoarded)
                        if(count <= 0) {
                            throw RuntimeException("Error updating student boarding status while cancelling")
                        }
                        val latitude: Double = lastKnownLocation?.latitude ?: -1.0
                        val longitude: Double = lastKnownLocation?.longitude ?: -1.0
                        val time: Long = lastKnownLocation?.time ?: -1L

                        val studentTransaction = CheckinTransaction(student.id, EntityType.STUDENT, -1, tripId, TransactionType.CANCEL_CHECKIN, mode, student.cardId,  student.isManualCheckin, latitude, longitude, time)
                        transactionsDao.insert(studentTransaction)
                        Timber.i("Current transaction: $studentTransaction")

                        if(student.isCrossBoarded && removeStudentTripRelationship) {
                            Timber.i("Student had crossboarded, removing relationship")
                            val relCount = studentsDao.deleteStudentTripRelationShip(student.id, tripId)
                            if(relCount <= 0) {
                                throw RuntimeException("Error deleting student-trip crossboarding relationship while cancelling")
                            }
                        }

                        if(cancelGuardians) {
                            val guardians = guardiansDao.getAllCheckedInGuardians(student.id)
                            for(guardian in guardians) {
                                guardian.checkinStatus = CheckinStatus.NOT_CHECKED_IN
                                val countGuardian = guardiansDao.updateCheckinStatus(student.id, guardian.id, guardian.checkinStatus, guardian.isManualCheckin)
                                if(countGuardian <= 0) {
                                    throw RuntimeException("Error updating guardian checkin status while cancelling")
                                }
                                val guardianTransaction = CheckinTransaction(guardian.id, EntityType.GUARDIAN, student.id, tripId, TransactionType.CANCEL_CHECKIN, mode, guardian.cardId, guardian.isManualCheckin, latitude, longitude, time)
                                transactionsDao.insert(guardianTransaction)
                                Timber.i("Current transaction: $guardianTransaction")
                            }
                        }
                    }
                }
                Timber.i("Student cancellation successful: $student")
                _studentUpdated.value = student
            } catch (e: Exception) {
                _errorUpdatingStudent.value = "Error updating student cancellation information"
                Timber.e("Error updating student cancellation: $student")
                Timber.e(e)
            }
        }
    }

    fun onStudentCheckinCancelled(student: Student) {
        if(tripStatus.value == Trip.TripStatus.TRIP_IN_PROGRESS) {
            // student.boardingStatus = if (student.boardingStatus == BoardingStatus.BOARDED) BoardingStatus.NOT_BOARDED else BoardingStatus.BOARDED
            student.boardingStatus = when(mode) {
                BoardingFragmentMode.MODE_PICKUP_BOARDING -> {
                    if (student.boardingStatus == BoardingStatus.BOARDED) BoardingStatus.NOT_BOARDED else student.boardingStatus
                }

                BoardingFragmentMode.MODE_DROP_BOARDING -> {
                    // if (student.boardingStatus == BoardingStatus.BOARDED) BoardingStatus.SCHEDULED_TO_OFF_BOARD else student.boardingStatus
                    BoardingStatus.BOARDED
                }

                BoardingFragmentMode.MODE_DROP_OFF -> {
                    if (student.boardingStatus == BoardingStatus.OFF_BOARDED) BoardingStatus.SCHEDULED_TO_OFF_BOARD else student.boardingStatus
                }

                BoardingFragmentMode.MODE_GATE_BOARDING -> {
                    if (student.boardingStatus == BoardingStatus.BOARDED) BoardingStatus.NOT_BOARDED else student.boardingStatus
                }

                BoardingFragmentMode.MODE_GATE_DROP_OFF -> {
                    BoardingStatus.BOARDED
                }
            }

            if((mode == BoardingFragmentMode.MODE_DROP_OFF || mode == BoardingFragmentMode.MODE_GATE_DROP_OFF)) {
                if(student.selfDropEnabled) {
                    cancelStudentUpdate(student, false, removeStudentTripRelationship = false)
                } else {
                    cancelStudentUpdate(student, true, removeStudentTripRelationship = false)
                }
            } else {
                cancelStudentUpdate(student, false, removeStudentTripRelationship = true)
            }
        } else {
            _tripNotStarted.value = true
            Timber.i("Trip has not started")
        }
    }

    fun toggleSort() {
        if(sortState.value == StudentListSortState.ALPHABETICAL) {
            sortState.value = StudentListSortState.BOARDING_STATUS
        } else {
            sortState.value = StudentListSortState.ALPHABETICAL
        }
    }

    override fun onCleared() {
        super.onCleared()
        studentViewModelJob.cancel()
    }

    enum class BoardingFragmentMode { MODE_PICKUP_BOARDING, MODE_DROP_BOARDING, MODE_DROP_OFF, MODE_GATE_BOARDING, MODE_GATE_DROP_OFF }

    enum class StudentListSortState { ALPHABETICAL, BOARDING_STATUS }
}