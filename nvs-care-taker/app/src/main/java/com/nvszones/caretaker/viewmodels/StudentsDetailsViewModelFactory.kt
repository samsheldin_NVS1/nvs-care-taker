package com.nvszones.caretaker.viewmodels

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nvszones.caretaker.dao.GuardiansDao
import com.nvszones.caretaker.dao.StudentsDao

class StudentsDetailsViewModelFactory (
    private val studentId: Long,
    private val tripId: Long,
    private val boardingFragmentMode: StudentViewModel.BoardingFragmentMode,
    private val studentsDao: StudentsDao,
    private val guardiansDao: GuardiansDao,
    private val application: Application ): ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(StudentDetailsViewModel::class.java)) {
            return StudentDetailsViewModel(studentId, tripId, boardingFragmentMode, studentsDao, guardiansDao, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}