package com.nvszones.caretaker.viewmodels

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nvszones.caretaker.dao.CareTakerDatabase

class StudentsViewModelFactory (
    private val mode: StudentViewModel.BoardingFragmentMode,
    private val tripId: Long,
    private val careTakerDatabase: CareTakerDatabase,
    private val application: Application ): ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(StudentViewModel::class.java)) {
            return StudentViewModel(mode, tripId, careTakerDatabase, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}