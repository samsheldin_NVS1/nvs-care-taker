package com.nvszones.caretaker.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.preference.PreferenceManager
import com.nvszones.caretaker.BuildConfig
import com.nvszones.caretaker.R
import com.nvszones.caretaker.dao.GuardiansDao
import com.nvszones.caretaker.dao.StudentsDao
import com.nvszones.caretaker.dao.TransactionsDao
import com.nvszones.caretaker.dao.TripsDao
import com.nvszones.caretaker.models.*
import com.nvszones.caretaker.network.NetworkUtils
import com.nvszones.caretaker.utils.LiveEvent
import kotlinx.coroutines.*
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import kotlin.random.Random

private const val PREF_KEY_LAST_SYNC_TIME = "lastSyncTime"

class SyncViewModel(private val routeId: Long, private val tripsDao: TripsDao, private val studentsDao: StudentsDao, private val guardiansDao: GuardiansDao, private val transactionsDao: TransactionsDao, private val app: Application): AndroidViewModel(app) {

    private val MAX_SYNC_THRESHOLD = 1000 * 60 * 30 // How long can you go without sync (in millis)

    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    val overallSyncStatus = MediatorLiveData<SyncStatus>()

    val tripsSyncStatus = MutableLiveData<SyncStatus>()

    val studentsSyncStatus = MutableLiveData<SyncStatus>()

    val guardiansSyncStatus = MutableLiveData<SyncStatus>()

    private val _networkFetchError = LiveEvent<String>()
    val eventNetworkFetchError: LiveData<String> = _networkFetchError

    val trips = tripsDao.getAllTrips(routeId)

    val students = studentsDao.getAllStudents()

    val guardians = guardiansDao.getAllBaseGuardians()

    // TODO: Sync reject for transactions, handle with timeouts

    val lastSyncTimeText: String
        get() {
            val lastSyncTime = PreferenceManager.getDefaultSharedPreferences(app).getLong(PREF_KEY_LAST_SYNC_TIME, -1L)
            return if(lastSyncTime <= 0) {
                app.getString(R.string.not_available)
            } else {
                try {
                    val date = Date(lastSyncTime)
                    val sdf = SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US)
                    sdf.format(date)
                } catch (e: Exception) {
                    Timber.e(e)
                    app.getString(R.string.not_available)
                }
            }
        }

    val isSyncNecessary: Boolean
        get() {
            val currentTime = System.currentTimeMillis()
            val lastSyncTime = PreferenceManager.getDefaultSharedPreferences(app).getLong(PREF_KEY_LAST_SYNC_TIME, -1L)
            return currentTime - lastSyncTime > MAX_SYNC_THRESHOLD && !BuildConfig.DEBUG // Sync not mandatory for dev builds
        }

    init {
        overallSyncStatus.addSource(tripsSyncStatus) {
            overallSyncStatus.value = syncState
        }
        overallSyncStatus.addSource(studentsSyncStatus) {
            overallSyncStatus.value = syncState
        }
        overallSyncStatus.addSource(guardiansSyncStatus) {
            overallSyncStatus.value = syncState
        }
        overallSyncStatus.value = SyncStatus.SYNC_NOT_STARTED
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    private val syncState: SyncStatus
        get() {
            return if(tripsSyncStatus.value == SyncStatus.SYNC_IN_PROGRESS || studentsSyncStatus.value == SyncStatus.SYNC_IN_PROGRESS || guardiansSyncStatus.value == SyncStatus.SYNC_IN_PROGRESS) {
                SyncStatus.SYNC_IN_PROGRESS
            } else if(tripsSyncStatus.value == SyncStatus.SYNC_FAILURE || studentsSyncStatus.value == SyncStatus.SYNC_FAILURE || guardiansSyncStatus.value == SyncStatus.SYNC_FAILURE) {
                SyncStatus.SYNC_FAILURE
            } else if(tripsSyncStatus.value == SyncStatus.SYNC_SUCCESS && studentsSyncStatus.value == SyncStatus.SYNC_SUCCESS || guardiansSyncStatus.value == SyncStatus.SYNC_SUCCESS) {
                SyncStatus.SYNC_SUCCESS
            } else if(tripsSyncStatus.value == SyncStatus.SYNC_STARTED || studentsSyncStatus.value == SyncStatus.SYNC_STARTED || guardiansSyncStatus.value == SyncStatus.SYNC_STARTED) {
                SyncStatus.SYNC_STARTED
            } else {
                SyncStatus.SYNC_NOT_STARTED
            }
        }

    fun saveSyncTime() {
        if(overallSyncStatus.value == SyncStatus.SYNC_SUCCESS) {
            PreferenceManager.getDefaultSharedPreferences(app).edit()
                .putLong(PREF_KEY_LAST_SYNC_TIME, System.currentTimeMillis()).apply()
        } else {
            throw RuntimeException("Save sync time called, but the overallSyncStatus is ${overallSyncStatus.value}")
        }
    }

    fun fetchTrips2() {
        tripsSyncStatus.value = SyncStatus.SYNC_IN_PROGRESS
        val trips = mutableListOf<Trip>()
        /*for (i in 0..10) {
            trips.add(Trip(i.toLong(), routeId,"Route $i", "Pickup $i", if (i%2==0) Trip.TRIP_TYPE_PICKUP else Trip.TRIP_TYPE_DROP, "2019-10-22T01:37:56", "2019-10-22T01:57:56"))
        }*/
        trips.add(Trip(0L, routeId,"Route 0", "Pickup 0", Trip.TripType.TRIP_TYPE_PICKUP, "2019-10-22T01:37:56", "2019-10-22T01:57:56"))
        trips.add(Trip(1L, routeId,"Route 1", "Pickup 1", Trip.TripType.TRIP_TYPE_DROP, "2019-10-22T01:37:56", "2019-10-22T01:57:56"))
        trips.add(Trip(2L, routeId,"Route 2", "Pickup 2", Trip.TripType.TRIP_TYPE_DROP, "2019-10-22T01:37:56", "2019-10-22T01:57:56"))
        trips.add(Trip(3L, routeId,"Route 3", "Pickup 3 Gate Entry", Trip.TripType.TRIP_TYPE_GATE_ENTRY, "2019-10-22T01:37:56", "2019-10-22T01:57:56"))
        trips.add(Trip(4L, routeId,"Route 4", "Pickup 4 Gate Exit", Trip.TripType.TRIP_TYPE_GATE_EXIT, "2019-10-22T01:37:56", "2019-10-22T01:57:56"))
        trips.add(Trip(5L, routeId,"Route 5", "Pickup 5", Trip.TripType.TRIP_TYPE_PICKUP, "2019-10-22T01:37:56", "2019-10-22T01:57:56"))
        trips.add(Trip(6L, routeId,"Route 6", "Pickup 6", Trip.TripType.TRIP_TYPE_DROP, "2019-10-22T01:37:56", "2019-10-22T01:57:56"))
        uiScope.launch {
            try {
                val successIds = withContext(Dispatchers.IO) {
                    tripsDao.clear()
                    tripsDao.insert(trips)
                }
                Timber.d("Inserted $successIds entries into trips table")
                if(successIds.isNotEmpty()) {
                    tripsSyncStatus.value = SyncStatus.SYNC_SUCCESS
                } else {
                    tripsSyncStatus.value = SyncStatus.SYNC_FAILURE
                }
            } catch (e: Exception) {
                tripsSyncStatus.value = SyncStatus.SYNC_FAILURE
                Timber.e("Error while inserting trip data to the database")
                Timber.e(e)
            }
        }
    }

    fun fetchStudents2() {
        studentsSyncStatus.value = SyncStatus.SYNC_IN_PROGRESS
        val students = mutableListOf<BaseStudent>()
        val relationship = mutableListOf<StudentTripRelationship>()
        for(i in 0..10) {
            val rand = Random.nextInt()
            students.add(BaseStudent(i.toLong(), "Student $i", "Class VI 'A' section", listOf(Gender.MALE, Gender.FEMALE).shuffled()[0], "wewrwewrr", "https://picsum.photos/200/300?random=$rand", Random.nextBoolean()))
        }
        students[0].cardId = "046C22CA304C80"
        students[8].cardId = "263581F4"
        students[2].cardId = "044E8D3A8F4981"
        students[0].selfDropEnabled = false
        students[1].selfDropEnabled = false
        students[2].selfDropEnabled = true
        for(i in 0..10) {
            relationship.add(StudentTripRelationship(i.toLong(), 0, BoardingStatus.NOT_BOARDED, false, isCrossBoarded = false))
            relationship.add(StudentTripRelationship(i.toLong(), 3, BoardingStatus.NOT_BOARDED, false, isCrossBoarded = false))
            relationship.add(StudentTripRelationship(i.toLong(), 4, BoardingStatus.NOT_BOARDED, false, isCrossBoarded = false))
        }
        for(i in 0..6) {
            relationship.add(StudentTripRelationship(i.toLong(), 1, BoardingStatus.NOT_BOARDED, false, isCrossBoarded = false))
        }
        for(i in 7..10) {
            relationship.add(StudentTripRelationship(i.toLong(), 2, BoardingStatus.NOT_BOARDED, false, isCrossBoarded = false))
        }
        uiScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    studentsDao.clear()
                    studentsDao.insert(students, relationship)
                }
                studentsSyncStatus.value = SyncStatus.SYNC_SUCCESS
            } catch (e: Exception) {
                studentsSyncStatus.value = SyncStatus.SYNC_FAILURE
                Timber.e("Error while inserting student data to the database")
                Timber.e(e)
            }
        }
    }

    fun fetchGuardians2() {
        guardiansSyncStatus.value = SyncStatus.SYNC_IN_PROGRESS
        val guardians = mutableListOf<BaseGuardian>()
        for(i in 0..30) {
            val rand = Random.nextInt()
            guardians.add(BaseGuardian(i.toLong(), "Guardian name $i", listOf(Gender.MALE, Gender.FEMALE).shuffled()[0], "asdafadsfasdf", "https://picsum.photos/200/300?random=$rand", "9090009909"))
        }
        guardians[0].cardId = "046C22CA304C80"
        guardians[5].cardId = "E6667CF4"

        val relationships = mutableListOf<StudentGuardianRelationship>()
        for(i in 0..10) {
            relationships.add(StudentGuardianRelationship(i.toLong(), (i*3+0).toLong(), "Father", true, CheckinStatus.NOT_CHECKED_IN, false))
            relationships.add(StudentGuardianRelationship(i.toLong(), (i*3+1).toLong(), "Uncle", false, CheckinStatus.NOT_CHECKED_IN, false))
            relationships.add(StudentGuardianRelationship(i.toLong(), (i*3+2).toLong(), "Uncle", false, CheckinStatus.NOT_CHECKED_IN, false))
        }
        uiScope.launch {
            try {
                withContext(Dispatchers.IO) {
                    guardiansDao.clear()
                    guardiansDao.insert(guardians, relationships)
                }
                guardiansSyncStatus.value = SyncStatus.SYNC_SUCCESS
            } catch (e: Exception) {
                guardiansSyncStatus.value = SyncStatus.SYNC_FAILURE
                Timber.e("Error while inserting guardian data to the database")
                Timber.e(e)
            }
        }
    }

    fun fetchTrips() {
        uiScope.launch {
            tripsSyncStatus.value = SyncStatus.SYNC_IN_PROGRESS
            try {
                val response = NetworkUtils.apiService.getTrips(routeId)
                if(response.isSuccessful) {
                    val trips = response.body()
                    Timber.i("Fetched trips (${response.code()}, ${response.isSuccessful}): $trips")
                    val successIds = withContext(Dispatchers.IO) {
                        tripsDao.clear()
                        tripsDao.insert(trips!!)
                    }
                    Timber.d("Inserted $successIds entries into trips table")
                    // TODO: Verify number of records inserted is equal to the records returned by the API. Same for students and guardians API
                    // if(successIds.size >= trips?.size?:0) {
                    tripsSyncStatus.value = SyncStatus.SYNC_SUCCESS
                    //} else {
                        // tripsSyncStatus.value = SyncStatus.SYNC_FAILURE
                    //}
                } else {
                    Timber.e("Error loading trips data: $response")
                    _networkFetchError.value = "Error loading trips data"
                    tripsSyncStatus.value = SyncStatus.SYNC_FAILURE
                }
            } catch (e: Exception) {
                _networkFetchError.value = "Please check your internet connection"
                tripsSyncStatus.value = SyncStatus.SYNC_FAILURE
                Timber.e("Error while inserting trip data to the database")
                Timber.e(e)
            }
        }
    }

    fun fetchStudents() {
        uiScope.launch {
            studentsSyncStatus.value = SyncStatus.SYNC_IN_PROGRESS
            try {
                Timber.i("Starting student response")
                val studentsResponse = NetworkUtils.apiService.getStudents(routeId)
                Timber.i("StudentResponse $studentsResponse")
                val relationshipResponse = NetworkUtils.apiService.getStudentTripRelationships(routeId)
                if(studentsResponse.isSuccessful && relationshipResponse.isSuccessful) {
                    val students = studentsResponse.body()
                    /*
                    // Hard code card id for a particular student for debugging
                    students?.map {
                        if(it.id == 1282L) {
                            it.cardId = "046C22CA304C80"
                        }
                        it
                    }*/
                    val relationships = relationshipResponse.body()
                    Timber.i("Fetched students (${studentsResponse.code()}, ${studentsResponse.isSuccessful}): $students")
                    Timber.i("Fetched student trip relationships (${relationshipResponse.code()}, ${relationshipResponse.isSuccessful}): $relationships")
                    withContext(Dispatchers.IO) {
                        studentsDao.clear()
                        studentsDao.insert(students!!, relationships!!)
                    }
                    studentsSyncStatus.value = SyncStatus.SYNC_SUCCESS
                } else {
                    Timber.e("Error loading students data: $studentsResponse")
                    Timber.e("Error loading student_trips data: $relationshipResponse")
                    _networkFetchError.value = "Error loading student data"
                    studentsSyncStatus.value = SyncStatus.SYNC_FAILURE
                }
            } catch (e: Exception) {
                _networkFetchError.value = "Please check your internet connection"
                studentsSyncStatus.value = SyncStatus.SYNC_FAILURE
                Timber.e("Error while inserting student data to the database")
                Timber.e(e)
            }
        }
    }

    fun fetchGuardians() {
        uiScope.launch {
            guardiansSyncStatus.value = SyncStatus.SYNC_IN_PROGRESS
            try {
                val guardianResponse = NetworkUtils.apiService.getGuardians(routeId)
                val relationshipResponse = NetworkUtils.apiService.getStudentGuardianRelationships(routeId)
                if(guardianResponse.isSuccessful && relationshipResponse.isSuccessful) {
                    val guardians = guardianResponse.body()
                    val relationships = relationshipResponse.body()
                    Timber.i("Fetched guardians (${guardianResponse.code()}, ${guardianResponse.isSuccessful}): $guardians")
                    Timber.i("Fetched guardian relationships (${relationshipResponse.code()}, ${relationshipResponse.isSuccessful}): $relationships")
                    withContext(Dispatchers.IO) {
                        guardiansDao.clear()
                        guardiansDao.insert(guardians!!, relationships!!)
                    }
                    guardiansSyncStatus.value = SyncStatus.SYNC_SUCCESS
                } else {
                    Timber.e("Error loading guardians data: $guardianResponse")
                    Timber.e("Error loading guardian relationships data: $relationshipResponse")
                    _networkFetchError.value = "Error loading trips data"
                    guardiansSyncStatus.value = SyncStatus.SYNC_FAILURE
                }
            } catch (e: Exception) {
                _networkFetchError.value = "Please check your internet connection"
                guardiansSyncStatus.value = SyncStatus.SYNC_FAILURE
                Timber.e("Error while inserting guardian data to the database")
                Timber.e(e)
            }
        }
    }

    fun clearTransactions() {
        uiScope.launch {
            withContext(Dispatchers.IO) {
                transactionsDao.clear()
            }
        }
    }

    fun clearData() {
        uiScope.launch {
            withContext(Dispatchers.IO) {
                tripsDao.clear()
                studentsDao.clear()
                guardiansDao.clear()
                transactionsDao.clear()
            }
        }
    }

    enum class SyncStatus {
        SYNC_NOT_STARTED,
        SYNC_STARTED,
        SYNC_IN_PROGRESS,
        SYNC_SUCCESS,
        SYNC_FAILURE,
    }
}