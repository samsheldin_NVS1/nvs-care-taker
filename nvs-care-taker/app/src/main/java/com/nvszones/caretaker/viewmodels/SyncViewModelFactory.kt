package com.nvszones.caretaker.viewmodels

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nvszones.caretaker.dao.GuardiansDao
import com.nvszones.caretaker.dao.StudentsDao
import com.nvszones.caretaker.dao.TransactionsDao
import com.nvszones.caretaker.dao.TripsDao

class SyncViewModelFactory (
    private val routeId: Long,
    private val tripsDao: TripsDao,
    private val studentsDao: StudentsDao,
    private val guardiansDao: GuardiansDao,
    private val transactionsDao: TransactionsDao,
    private val application: Application ): ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(SyncViewModel::class.java)) {
            return SyncViewModel(routeId, tripsDao, studentsDao, guardiansDao, transactionsDao, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}