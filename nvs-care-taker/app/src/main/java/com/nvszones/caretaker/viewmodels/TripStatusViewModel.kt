package com.nvszones.caretaker.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nvszones.caretaker.models.Trip

class TripStatusViewModel(): ViewModel() {
    val tripStatus = MutableLiveData<Trip.TripStatus>()

    init {
        tripStatus.value = Trip.TripStatus.TRIP_NOT_STARTED
    }
}