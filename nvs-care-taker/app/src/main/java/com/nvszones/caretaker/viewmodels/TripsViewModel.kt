package com.nvszones.caretaker.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.nvszones.caretaker.dao.TripsDao

class TripsViewModel(val tripsDao: TripsDao, application: Application): AndroidViewModel(application) {

    val trips = tripsDao.getAllTrips()
}