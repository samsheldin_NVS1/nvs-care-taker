package com.nvszones.caretaker.viewmodels

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nvszones.caretaker.dao.TripsDao

class TripsViewModelFactory (
    private val dataSource: TripsDao,
    private val application: Application ): ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(TripsViewModel::class.java)) {
            return TripsViewModel(dataSource, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}